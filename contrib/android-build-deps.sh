#!/bin/sh
#
# Build libpwmd dependencies for Android. The order of building the
# dependencies usuually goes: libgpg-error, libassuan, openssl, libssh2,
# libpwmd. You may need to adjust the following paths for your own
# environment.
#
# Ben Kibbey <bjk@luxsci.net>

# Where to install cross-compiled dependencies.
INSTALL_PREFIX="$HOME/projects/android/local"

# Target Android API level. This is to determine which cross-compiler version
# to use.
ANDROID_API_LEVEL="34"

# Usually the same as where Android Studio installs SDK/NDK versions. This is
# the NDK version to use and usually corrosponds to the Android API level.
ANDROID_NDK_ROOT="$HOME/projects/android/sdk/ndk/26.1.10909125"

# Where to find the cross-compilers.
NDK_BIN_DIR="$ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/linux-x86_64/bin"

PATH=$NDK_BIN_DIR:$PATH
export ANDROID_NDK_ROOT

usage() {
    cat << EOF 1>&2
Usage: $0 [--strip] [--ssl] | --symlinks -- all | armv7a aarch64 i686 x86_64
    --strip    Strip installed binaries
    --ssl      Special option when building OpenSSL
    --symlinks Create well known architecture symlinks
EOF
    exit 1
}

TEMP=$(getopt -o 'slSh' --long 'strip,ssl,symlinks,help' -- "$@")
if [ $? -ne 0 ]; then
    usage
fi

eval set -- "$TEMP"
unset TEMP

while true; do
    case "$1" in
	'-s'|'--strip')
	    STRIP=1
	    shift
	    ;;
	'-l'|'--ssl')
	    SSL=1
	    shift
	    ;;
	'-S'|'--symlinks')
	    SYMLINKS=1
	    shift
	    ;;
	'-h'|'--help')
	    usage
	    ;;
	'--')
	    shift
	    break
	    ;;
	*)
	    usage
	    ;;
    esac
done

if [ x"$SYMLINKS" != "x" ]; then
    todo="armv7a aarch64 i686 x86_64"

    set -e
    for arch in $todo; do
	mkdir -p $INSTALL_PREFIX/$arch
	cd $INSTALL_PREFIX

	if [ $arch = "aarch64" -a ! -L arm64-v8a ]; then
	    ln -sf $arch arm64-v8a
	elif [ $arch = "i686" -a ! -L x86 ]; then
	    ln -sf $arch x86
	elif [ $arch = "armv7a" -a ! -L armeabi-v7a ]; then
	    ln -sf $arch armeabi-v7a
	fi
    done

    exit 0
fi

strip_libs() {
    for arch in $@; do
	$NDK_BIN_DIR/llvm-strip $INSTALL_PREFIX/$arch/lib/*.so
    done
}

todo="$@"

if [ x"$todo" = "x" ]; then
    usage
fi

for arch in "$@"; do
    case "$arch" in
	armv7a)
	    ;;
	aarch64)
	    ;;
	i686)
	    ;;
	x86_64)
	    ;;
	all)
	    todo="armv7a aarch64 i686 x86_64"
	    break
	    ;;
	*)
	    usage
	    ;;
    esac
done

set -e

for arch in $todo; do
    if [ x"$SSL" != "x" ]; then
	if [ "$arch" = "armv7a" ]; then
	    ARCH="android-arm"
	elif [ "$arch" = "aarch64" ]; then
	    ARCH="android-arm64"
	elif [ "$arch" = "i686" ]; then
	    ARCH="android-x86"
	elif [ "$arch" = "x86_64" ]; then
	    ARCH="android-x86_64"
	else
	    usage
	fi

	if [ ! -f "Configure" ]; then
	    echo "$0: ./Configure not found. Are you in the OpenSSL source rootdir?" 1>&2
	    exit 1
	fi

	./Configure -no-docs --prefix=$INSTALL_PREFIX/$arch $ARCH && \
	    make -j8 && \
	    make install && \
	    make distclean

	if [ $? -ne 0 ]; then
	    exit 1
	fi

	if [ x"$STRIP" != "x" ]; then
	    strip_libs $arch
	fi

	continue
    fi

    if [ "$arch" = "armv7a" ]; then
	ARCHCC="armv7a-linux-androideabi"
    else
	ARCHCC="$arch-linux-android"
    fi

    PATH="$INSTALL_PREFIX/$arch/bin:$PATH" \
    PKG_CONFIG_PATH="$INSTALL_PREFIX/$arch/lib/pkgconfig" \
    CONFIG_SITE=/dev/null \
    CC="${ARCHCC}${ANDROID_API_LEVEL}-clang" \
    ./configure --host="$arch-linux-androideabi" \
      --enable-maintainer-mode \
      --disable-interactive \
      --enable-ssh \
      --disable-pinentry \
      --with-libgpg-error-prefix=$INSTALL_PREFIX/$arch \
      --with-libassuan-prefix=$INSTALL_PREFIX/$arch \
      --prefix=$INSTALL_PREFIX/$arch \
      --disable-languages \
      --with-crypto=openssl \
      --with-libssl-prefix=$INSTALL_PREFIX/$arch && \
      make -j8 install && \
      make distclean

      if [ x"$STRIP" != "x" ]; then
	  strip_libs $arch
      fi
done
