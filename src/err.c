/*
    Copyright (C) 2018-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License version 2.1 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <errno.h>

int err (int rc, const char *fmt, ...)
{
  va_list ap, cp;
  char *buf = NULL;
  size_t n;

  va_start (ap, fmt);
  va_copy (cp, ap);
  n = vsnprintf (NULL, 0, fmt, cp);
  va_end (cp);
  va_copy (cp, ap);
  buf = malloc (n+1);
  if (!buf)
    {
      fprintf (stderr, "%s\n", strerror (ENOMEM));
      exit (EXIT_FAILURE);
    }

  n = vsnprintf (buf, n, fmt, cp);
  va_end (cp);
  va_end (ap);
  fprintf (stderr, "%s", buf);
  free (buf);
  exit (rc);
}
