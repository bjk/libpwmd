/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License version 2.1 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif

#ifndef LINE_MAX
#define LINE_MAX	2048
#endif

#include <libpwmd.h>
#include "misc.h"

#ifndef __MINGW32__
#ifdef HAVE_GETPWUID_R
char *
_expand_homedir (char *str, struct passwd *pw)
{
  char *p = str;
  char *pwbuf = NULL;
  char *result;
  struct passwd t;

  if (!p)
    return NULL;

  if (*p != '~' || *(p + 1) != '/')
    return pwmd_strdup (p);

  if (!pw)
    {
      pwbuf = _getpwuid (&t);
      if (!pwbuf)
	return NULL;

      pw = &t;
    }

  p += 2;
  result = pwmd_strdup_printf ("%s/%s", pw->pw_dir, p);
  pwmd_free (pwbuf);
  return result;
}

char *
_getpwuid (struct passwd *pwd)
{
  size_t size = sysconf (_SC_GETPW_R_SIZE_MAX);
  struct passwd *result;
  char *buf;
  int n;

  if (size == -1)
    size = 16384;

  buf = pwmd_malloc (size);
  if (!buf)
    return NULL;

  n = getpwuid_r (getuid (), pwd, buf, size, &result);
  if (n)
    {
      pwmd_free (buf);
      errno = n;
      return NULL;
    }

  if (!result)
    {
      pwmd_free (buf);
      return NULL;
    }

  errno = n;
  return buf;
}
#else
char *
_expand_homedir (char *str, struct passwd *pw)
{
  char *p = str;

  if (!p)
    return NULL;

  if (*p != '~' || *(p + 1) != '/')
    return pwmd_strdup (p);

  if (!pw)
    {
      pw = getpwuid (getuid ());
      if (!pw)
	return NULL;
    }

  p += 2;
  return pwmd_strdup_printf ("%s/%s", pw->pw_dir, p);
}

char *
_getpwuid (struct passwd *pwd)
{
  struct passwd *pw = getpwuid (getuid ());

  if (!pw)
    return NULL;

  *pwd = *pw;
  return pwmd_strdup ("");
}
#endif
#endif

/*
 * Borrowed from libassuan.
 */
char *
_percent_escape (const char *atext)
{
  const unsigned char *s;
  int len;
  char *buf, *p;

  if (!atext)
    return NULL;

  len = strlen (atext) * 3 + 1;
  buf = (char *) pwmd_malloc (len);

  if (!buf)
    return NULL;

  p = buf;

  for (s = (const unsigned char *) atext; *s; s++)
    {
      if (*s < ' ')
	{
	  sprintf (p, "%%%02X", *s);
	  p += 3;
	}
      else
	*p++ = *s;
    }

  *p = 0;
  return buf;
}

/* Common hostname parsing for urls. Handles both IPv4 and IPv6 hostname and
 * port specification.
 */
gpg_error_t
parse_hostname_common (const char *str, char **host, int *port)
{
  const char *p = str;
  char *t = NULL;

  /* IPv6 with optional port. */
  if (*p == '[')
    {
      p++;
      t = strchr (p, ']');
    }
  else
    {
      int n = 0;
      const char *x;

      /* Handle IPv6 without proper braces around the IP. */
      for (x = p; *x; x++)
	{
	  if (*x == ':')
	    n++;
	}

      if (n <= 1)
	t = strchr (p, ':');
    }

  if (t)
    {
      size_t len = strlen (p) - strlen (t) + 1;

      *host = pwmd_malloc (len);
      if (!*host)
	return gpg_error_from_errno (ENOMEM);

      snprintf (*host, len, "%s", p);
      t++;

      if (*t == ':')
	t++;

      if (*t)
	*port = strtol (t, NULL, 10);

      if (*t == '-')
	t++;

      while (*t && isdigit (*t))
	t++;
    }
  else
    {
      *host = pwmd_strdup (str);
      if (!*host)
	return gpg_error_from_errno (ENOMEM);
    }

  return 0;
}

char *
bin2hex (const unsigned char *data, size_t len)
{
  size_t size = len * 2 + 1;
  char buf[size]; // C99
  size_t n;

  buf[0] = 0;

  for (n = 0; n < len; n++)
    {
      char c[3];

      sprintf (c, "%02X", data[n]);
      strcat (buf, c);
    }

  return pwmd_strdup (buf);
}

gpg_error_t
set_non_blocking (assuan_fd_t fd, int n)
{
#ifdef __MINGW32__
  unsigned long opt = !!n;

  if (ioctlsocket (HANDLE2SOCKET (fd), FIONBIO, &opt) != NO_ERROR)
    return gpg_error_from_syserror ();
#else
  int flags = fcntl (fd, F_GETFL);

  if (!n)
    flags &= ~O_NONBLOCK;
  else
    flags |= O_NONBLOCK;

  if (fcntl (fd, F_SETFL, flags) == -1)
    return gpg_error_from_syserror ();
#endif

  return 0;
}
