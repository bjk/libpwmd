/*
    Copyright (C) 2012-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License version 2.1 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef UTIL_SLIST_H
#define UTIL_SLIST_H

struct slist_s
{
  void *data;
  struct slist_s *next;
};

unsigned slist_length (struct slist_s *list);
void *slist_nth_data (struct slist_s *list, unsigned n);
struct slist_s *slist_append (struct slist_s *list, void *data);
void slist_free (struct slist_s *list);
struct slist_s *slist_remove (struct slist_s *list, void *data);

#endif
