/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License version 2.1 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <stdarg.h>
#include <time.h>

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif

#ifndef __MINGW32__
#    ifdef HAVE_TERMIOS_H
#    include <termios.h>
#    endif
#    ifdef HAVE_SYS_SOCKET_H
#    include <sys/socket.h>
#    endif
#    include <sys/un.h>
#    include <sys/wait.h>
#    include <pwd.h>
#    include <sys/select.h>
#    ifdef HAVE_ERR_H
#    include <err.h>
#    endif
#endif

#include <libpwmd.h>
#include "util-string.h"

#ifndef LINE_MAX
#define LINE_MAX	2048
#endif

#include "mem.h"
#include "misc.h"
#include "types.h"
#include "version.h"

#ifdef WITH_PINENTRY
#include "pinentry.h"
#endif

#ifdef WITH_QUALITY
#include "zxcvbn.h"
#endif

#if defined(WITH_SSH) || defined(WITH_GNUTLS)
#include <sys/types.h>
#    ifndef __MINGW32__
#        ifdef HAVE_SYS_SOCKET_H
#        include <sys/socket.h>
#        endif
#        ifdef HAVE_NETDB_H
#        include <netdb.h>
#        endif
#        ifdef HAVE_NETINET_IN_H
#        include <netinet/in.h>
#        endif
#    endif
#endif

enum
  {
    PWMD_WHICH_NONE,
    PWMD_WHICH_SAVE,
    PWMD_WHICH_PASSWD,
    PWMD_WHICH_GENKEY
  };

#define TEST_BULK_OFFSET(a, b) (a -= b == a ? 0 : 1)
#define FINISH(rc) (gpg_err_source(rc) == GPG_ERR_SOURCE_UNKNOWN) \
					? gpg_error(rc) : rc

typedef struct
{
  size_t len;
  void *buf;
  pwm_t *pwm;
} membuf_t;

struct bulk_inquire_s
{
  pwm_t *pwm;
  const char *data;
  size_t size;
  void *user;
  pwmd_inquire_cb_t func;
};

static gpg_error_t status_cb (void *data, const char *line);

ssize_t
hook_read (assuan_context_t ctx, assuan_fd_t fd, void *data, size_t len)
{
  pwm_t *pwm = assuan_get_pointer (ctx);

  if (pwm->read_cb)
    return pwm->read_cb (pwm->read_cb_data, HANDLE2SOCKET (fd), data, len);

#if defined(WITH_SSH) || defined(WITH_GNUTLS)
#ifdef WITH_SSH
  if (pwm && pwm->tcp && pwm->tcp->ssh)
    {
      ssize_t ret = read_hook_ssh (pwm->tcp->ssh, fd, data, len);

      if (ret == -1)
        {
	  int e = errno;

	  __assuan_close (ctx, pwm->fd);
	  pwm->fd = ASSUAN_INVALID_FD;
	  errno = e;
	}

      return ret;
    }
#endif
#ifdef WITH_GNUTLS
  if (pwm && pwm->tcp && pwm->tcp->tls)
    return tls_read_hook (pwm, fd, data, len);
#endif
#endif

  return read (HANDLE2SOCKET (fd), data, len);
}

ssize_t
hook_write (assuan_context_t ctx, assuan_fd_t fd, const void *data,
	    size_t len)
{
  pwm_t *pwm = assuan_get_pointer (ctx);
  ssize_t wrote;

  if (pwm->write_cb)
    return pwm->write_cb (pwm->write_cb_data, HANDLE2SOCKET (fd), data, len);

#if defined(WITH_SSH) || defined(WITH_GNUTLS)
#ifdef WITH_SSH
  if (pwm && pwm->tcp && pwm->tcp->ssh)
    {
      ssize_t ret = write_hook_ssh (pwm->tcp->ssh, fd, data, len);

      if (ret == -1)
        {
	  int e = errno;

	  __assuan_close (ctx, pwm->fd);
	  pwm->fd = ASSUAN_INVALID_FD;
	  errno = e;
	}

      return ret;
    }
#endif
#ifdef WITH_GNUTLS
  if (pwm && pwm->tcp && pwm->tcp->tls)
    return tls_write_hook (pwm, fd, data, len);
#endif
#endif

  /* libassuan cannot handle EAGAIN when doing writes. */
  do
    {
      wrote = write (HANDLE2SOCKET (fd), data, len);
      if (wrote == -1 && errno == EAGAIN)
	__assuan_usleep (pwm->ctx, 50000);
    }
  while (wrote == -1 && errno == EAGAIN);

  return wrote;
}

#ifndef __MINGW32__
pid_t
hook_waitpid (assuan_context_t ctx, pid_t pid, int action, int *status,
	      int options)
{
  (void)ctx;
  (void)action;
  return waitpid (pid, status, options);
}
#endif

gpg_error_t
pwmd_init ()
{
  static int initialized;

#ifdef WITH_GNUTLS
  // May be called more than once.
  gnutls_global_init ();
#endif

  if (initialized)
    return 0;

#ifdef ENABLE_NLS
  bindtextdomain ("libpwmd", LOCALEDIR);
#endif
#ifdef WITH_SSH
  libssh2_init (0);
#endif
#ifndef MEM_DEBUG
  gpgrt_set_alloc_func (_xrealloc_gpgrt);
#endif
  gpgrt_init ();

#ifdef WITH_QUALITY
  ZxcvbnInit (NULL);
#endif

  initialized = 1;
  return 0;
}

void
pwmd_deinit ()
{
  gpg_err_deinit (0);
#ifdef WITH_GNUTLS
  gnutls_global_deinit ();
#endif
#ifdef WITH_QUALITY
  ZxcvbnUnInit();
#endif
}

static gpg_error_t
bulk_inquire_cb (void *user, const char *keyword, gpg_error_t rc,
                    char **data, size_t * size)
{
  struct bulk_inquire_s *inq = user;
  pwm_t *pwm = inq->pwm;

  if (pwm->bulk_id && inq->func && inq->func != bulk_inquire_cb)
    {
      rc = inq->func (inq->user, keyword, rc, data, size);
      return rc;
    }
  else if (pwm->bulk_id)
    return rc ? rc : GPG_ERR_ASS_NO_INQUIRE_CB;

  if (rc)
    return rc;

  *data = (char *)inq->data;
  *size = inq->size;
  return GPG_ERR_EOF;
}

gpg_error_t
_connect_finalize (pwm_t * pwm)
{
  gpg_error_t rc = 0;
  assuan_fd_t active[2];
  int n = assuan_get_active_fds (pwm->ctx, 0, active, N_ARRAY (active));
  char *bulk = NULL;
  size_t offset = 0, toffset;
  gpg_error_t rcs[] = { 0, GPG_ERR_MISSING_ERRNO };

  if (n <= 0)
    return GPG_ERR_EBADFD;

  pwm->fd = active[0];
#ifdef WITH_PINENTRY
  pwm->pinentry_pid = -1;
#endif

  rc = pwmd_bulk_append (&bulk, "VERSION", strlen ("VERSION"), "GETINFO",
                         "VERSION", strlen ("VERSION"), &offset);
  toffset = offset;
  if (!rc && (pwm->opts & OPT_STATE_STATUS))
    {
      char *buf = pwmd_strdup_printf ("client-state=1");

      if (buf)
        rc = pwmd_bulk_append_rc (&bulk, rcs, "STATE", 5, "OPTION", buf,
                                  strlen (buf), &offset);
      else
        rc = GPG_ERR_ENOMEM;

      pwmd_free (buf);
    }

  if (!rc && pwm->name)
    {
      char *buf = pwmd_strdup_printf ("NAME=%s", pwm->name);

      TEST_BULK_OFFSET (offset, toffset);
      if (buf)
        rc = pwmd_bulk_append_rc (&bulk, rcs, "NAME", strlen ("NAME"), "OPTION",
                                  buf, strlen (buf), &offset);
      else
          rc = GPG_ERR_ENOMEM;

      pwmd_free (buf);
    }

  if (!rc)
    rc = pwmd_bulk_finalize (&bulk);

  if (!rc)
    {
      char *result = NULL;
      size_t size;

      rc = pwmd_bulk (pwm, &result, &size, NULL, NULL, bulk, strlen (bulk));
      pwmd_free (bulk);
      bulk = NULL;
      if (!rc)
        {
          const char *r;
          gpg_error_t rrc;
          size_t rsize;

          offset = 0;
          rc = pwmd_bulk_result (result, size, "STATE", 5, &offset, &r,
                                 &rsize, &rrc);
          if (gpg_err_code (rc) == GPG_ERR_NOT_FOUND)
            rc = 0;
          else if (!rc)
            rc = rrc;

          if (!rc)
            rc = pwmd_bulk_result (result, size, "VERSION", 7, &offset, &r,
                                   &rsize, &rrc);
          if (!rc && !rrc)
            pwm->version = strtoul (r, NULL, 16);

          if (!rc && !rrc)
            rc = pwmd_bulk_result (result, size, "NAME", 4, &offset, &r,
                                   &rsize, &rrc);

          if (gpg_err_code (rc) == GPG_ERR_NOT_FOUND)
            rc = 0;

          if (!rc)
            rc = rrc;

          pwmd_free (result);
        }
    }

  pwmd_free (bulk);
  return rc;
}

static gpg_error_t
connect_uds (pwm_t * pwm, const char *path)
{
#ifdef __MINGW32__
  return GPG_ERR_UNSUPPORTED_PROTOCOL;
#else
  if (!pwm)
    return GPG_ERR_INV_ARG;

  char *socketpath = NULL;
  struct passwd pw;
  char *pwbuf;
  gpg_error_t rc;

  pwbuf = _getpwuid (&pw);
  if (!pwbuf)
    return gpg_error_from_syserror ();

  if (!path || !*path)
    socketpath = pwmd_strdup_printf ("%s/.pwmd/socket", pw.pw_dir);
  else
    socketpath = _expand_homedir ((char *) path, &pw);

  pwmd_free (pwbuf);
  if (!socketpath)
    return GPG_ERR_ENOMEM;

  rc = assuan_socket_connect (pwm->ctx, socketpath, ASSUAN_INVALID_FD, 0);
  pwmd_free (socketpath);
  return rc ? rc : _connect_finalize (pwm);
#endif
}

static gpg_error_t
init_handle (pwm_t * pwm)
{
  gpg_error_t rc;
  static struct assuan_malloc_hooks mhooks = {
    pwmd_malloc, pwmd_realloc, pwmd_free
  };
  static struct assuan_system_hooks shooks = {
    ASSUAN_SYSTEM_HOOKS_VERSION,
    __assuan_usleep,
    __assuan_pipe,
    __assuan_close,
    hook_read,
    hook_write,
    //FIXME
    NULL,			//recvmsg
    NULL,			//sendmsg both are used for FD passing
    __assuan_spawn,
#ifdef __MINGW32__
    __assuan_waitpid,
#else
    hook_waitpid,
#endif
    __assuan_socketpair,
    __assuan_socket,
    __assuan_connect
  };

  rc = assuan_new_ext (&pwm->ctx, GPG_ERR_SOURCE_DEFAULT, &mhooks, NULL,
		       NULL);
  if (rc)
    return rc;

  assuan_set_pointer (pwm->ctx, pwm);
  assuan_ctx_set_system_hooks (pwm->ctx, &shooks);
  assuan_set_flag (pwm->ctx, ASSUAN_CONFIDENTIAL, 1);
  return 0;
}

#if defined(WITH_SSH) || defined(WITH_GNUTLS)
void
free_tcp (pwm_t *pwm)
{
  struct tcp_s *tcp = pwm->tcp;

  if (!tcp)
    return;

#ifdef WITH_SSH
  _free_ssh_conn (tcp->ssh);
#endif
#ifdef WITH_GNUTLS
  tls_free (pwm);
#endif

  pwmd_free (tcp->host);
  if (tcp->addrs)
    {
      freeaddrinfo (tcp->addrs);
      tcp->addrs = NULL;
    }

  pthread_cond_destroy (&tcp->dns_cond);
  pthread_mutex_destroy (&tcp->dns_mutex);
  pwmd_free (tcp);
  pwm->tcp = NULL;
}

static gpg_error_t
lookup_host (pwm_t *pwm)
{
  struct addrinfo hints = { 0 };
  char portstr[6];
  int n;

  switch (pwm->prot)
    {
    case PWMD_IP_ANY:
      hints.ai_family = AF_UNSPEC;
      break;
    case PWMD_IPV4:
      hints.ai_family = AF_INET;
      break;
    case PWMD_IPV6:
      hints.ai_family = AF_INET6;
      break;
    }

  hints.ai_socktype = SOCK_STREAM;
  snprintf (portstr, sizeof (portstr), "%i", pwm->tcp->port);
  n = getaddrinfo (pwm->tcp->host, portstr, &hints, &pwm->tcp->addrs);
  if (!n)
    return 0;

  switch (n)
    {
#ifndef __MINGW32__
    case EAI_SYSTEM:
      return gpg_error_from_syserror ();
#endif
    case EAI_AGAIN:
      return GPG_ERR_EAGAIN;
    case EAI_MEMORY:
      return GPG_ERR_ENOMEM;
    case EAI_NONAME:
      return GPG_ERR_UNKNOWN_HOST;
    default:
      break;
    }

  return GPG_ERR_DNS_UNKNOWN;
}

#ifndef __MINGW32__
static void *
resolve_host_thread (void *arg)
{
  pwm_t *pwm = arg;
  gpg_error_t *n = pwmd_malloc (sizeof (gpg_error_t));

  pthread_cleanup_push (pwmd_free, n);
  *n = lookup_host (pwm);
  pthread_cleanup_pop (0);
  pthread_cond_broadcast (&pwm->tcp->dns_cond);
  pwm->tcp->flags |= TCP_FLAG_DNS_FINISHED;
  pthread_exit (n);
  return n;
}
#endif

gpg_error_t
tcp_connect_common (pwm_t * pwm)
{
  gpg_error_t rc = 0;
  int n;
#ifndef __MINGW32__
#define TS_TIMEOUT 50000000L
  pthread_t tid;
  time_t now;

  pwm->tcp->flags &= ~TCP_FLAG_DNS_FINISHED;
  time (&now);
  n = pthread_create (&tid, NULL, resolve_host_thread, pwm);
  if (n)
    return gpg_error_from_errno (n);

  pthread_mutex_lock (&pwm->tcp->dns_mutex);

  for (;;)
    {
      struct timespec ts;
      gpg_error_t *result = NULL;

      clock_gettime (CLOCK_REALTIME, &ts);
      if (ts.tv_nsec + TS_TIMEOUT >= 1000000000LL) {
          ts.tv_sec++;
          ts.tv_nsec = 0;
      }
      else
        ts.tv_nsec += TS_TIMEOUT;

      if (pwm->cancel)
        {
#ifdef HAVE_PTHREAD_CANCEL
          pthread_cancel (tid);
          pthread_join (tid, NULL);
#else
          pthread_join (tid, (void **)&result);
          pwmd_free (result);
#endif
          return GPG_ERR_CANCELED;
        }

      n = pthread_cond_timedwait (&pwm->tcp->dns_cond, &pwm->tcp->dns_mutex,
                                  &ts);
      if (n == ETIMEDOUT && !(pwm->tcp->flags & TCP_FLAG_DNS_FINISHED))
        {
          if (pwm->socket_timeout && ts.tv_sec - now >= pwm->socket_timeout)
            {
#ifdef HAVE_PTHREAD_CANCEL
              pthread_cancel (tid);
              pthread_join (tid, NULL);
#else
              pthread_join (tid, (void **)&result);
              pwmd_free (result);
#endif
              return GPG_ERR_ETIMEDOUT;
            }

          continue;
        }
      else if (n && n != ETIMEDOUT)
        {
#ifdef HAVE_PTHREAD_CANCEL
          pthread_cancel (tid);
          pthread_join (tid, NULL);
#else
          pthread_join (tid, (void **)&result);
          pwmd_free (result);
#endif
          return gpg_error_from_errno (n);
        }

      /* It shouldn't happen, but it does. Noticable when running under
       * Valgrind: the pthread_cond_broadcast() is never heard yet the thread
       * returns with the value set. */
      if (pwm->tcp->flags & TCP_FLAG_DNS_FINISHED)
        {
          pthread_join (tid, (void **)&result);
          rc = *result;
          pwmd_free (result);
          break;
        }
    }
#else
  rc = lookup_host (pwm);
#endif

  if (rc)
    return rc;

  for (pwm->tcp->addr = pwm->tcp->addrs; pwm->tcp->addr;
       pwm->tcp->addr = pwm->tcp->addrs->ai_next)
    {
#ifdef __MINGW32__
      pwm->fh = _open_osfhandle (socket (pwm->tcp->addr->ai_family, SOCK_STREAM, 0), 0);
      pwm->fd = SOCKET2HANDLE (_get_osfhandle (pwm->fh));
#else
      pwm->fd = socket (pwm->tcp->addr->ai_family, SOCK_STREAM, 0);
#endif
      if (pwm->fd == ASSUAN_INVALID_FD)
	{
	  rc = gpg_error_from_syserror ();
	  if (pwm->tcp->addr == pwm->tcp->addrs->ai_next)
	    break;
	  continue;
	}

      rc = set_non_blocking (pwm->fd, 1);
      if (rc)
        {
	  __assuan_close (pwm->ctx, pwm->fd);
	  pwm->fd = ASSUAN_INVALID_FD;
	  break;
        }

      if (connect (HANDLE2SOCKET (pwm->fd), pwm->tcp->addr->ai_addr,
		   pwm->tcp->addr->ai_family == AF_INET6
		   ? sizeof (struct sockaddr_in6)
		   : sizeof (struct sockaddr)) == -1)
	{
	  struct timeval tv;
	  fd_set wfds;
          unsigned elapsed = 0;

	  rc = gpg_error_from_syserror ();
#ifdef __MINGW32__
	  int w = WSAGetLastError ();
	  switch (w)
	    {
	      case WSAEINPROGRESS:
              case WSAEWOULDBLOCK:
		rc = GPG_ERR_EINPROGRESS;
		break;
              default:
                break;
            }
#endif

	  if (gpg_err_code (rc) != GPG_ERR_EINPROGRESS)
	    {
	      __assuan_close (pwm->ctx, pwm->fd);
	      pwm->fd = ASSUAN_INVALID_FD;
	      if (pwm->tcp->addr == pwm->tcp->addrs->ai_next)
		return rc;
              continue;
	    }

again:
          tv.tv_sec = 1;
          tv.tv_usec = 0;
	  FD_ZERO (&wfds);
	  FD_SET (HANDLE2SOCKET (pwm->fd), &wfds);
	  n = select (HANDLE2SOCKET (pwm->fd)+1, NULL, &wfds, NULL, &tv);
	  rc = 0;
	  if (!n || pwm->cancel)
            {
              if (pwm->cancel)
                rc = gpg_error (GPG_ERR_CANCELED);
              else if (++elapsed >= pwm->socket_timeout)
                rc = gpg_error (GPG_ERR_ETIMEDOUT);
              else
                goto again;
            }
	  else if (n != -1)
	    {
	      socklen_t len = sizeof (int);
#ifdef __MINGW32__
	      char ret;

	      len = sizeof (char);
	      getsockopt (HANDLE2SOCKET (pwm->fd), SOL_SOCKET, SO_ERROR, &ret, &len);
	      if (ret)
		rc = gpg_error_from_errno (ret);
#else
	      getsockopt (pwm->fd, SOL_SOCKET, SO_ERROR, &n, &len);
	      if (n)
		rc = gpg_error_from_errno (n);
#endif
	    }
	  else if (n == -1)
	    rc = gpg_error_from_syserror ();

	  if (rc)
	    {
	      __assuan_close (pwm->ctx, pwm->fd);
	      pwm->fd = ASSUAN_INVALID_FD;
	      if (pwm->tcp->addr == pwm->tcp->addrs->ai_next
		  || gpg_err_code (rc) == GPG_ERR_ETIMEDOUT
                  || pwm->cancel)
		return rc;
	    }
	  else
	    break;
	}
      else
	break;
    }

  if (!rc)
    {
      rc = set_non_blocking (pwm->fd, 0);
      if (rc)
        {
	  __assuan_close (pwm->ctx, pwm->fd);
	  pwm->fd = ASSUAN_INVALID_FD;
	}
    }

  return rc;
}
#endif

static void
command_start (pwm_t *pwm)
{
  pwm->pin_repeated = 0;
  pwm->cancel = 0;
}

gpg_error_t
pwmd_connect_fd (pwm_t * pwm, int fd)
{
  gpg_error_t rc = 0;

  if (!pwm)
    return FINISH (GPG_ERR_INV_ARG);
  else if (!pwm->ctx)
    {
      rc = init_handle (pwm);
      if (rc)
        return FINISH (rc);
    }
  else if (pwm->fd != ASSUAN_INVALID_FD)
    return FINISH (GPG_ERR_INV_STATE);

  command_start (pwm);

#ifndef __MINGW32__
  if (!(pwm->opts & OPT_SIGPIPE))
    {
      struct sigaction sa = { 0 };

      sa.sa_handler = SIG_IGN;
      sigaction (SIGPIPE, &sa, NULL);
    }
#endif

  rc = assuan_socket_connect_fd (pwm->ctx, fd, 0);
  if (!rc)
    {
      pwm->fd = SOCKET2HANDLE (fd);
      pwm->connected = pwm->user_fd = 1;
      rc = _connect_finalize (pwm);
    }

  if (rc)
    pwmd_disconnect (pwm);

  return FINISH (rc);
}

gpg_error_t
pwmd_connect (pwm_t * pwm, const char *url, ...)
{
  const char *p = url;
  gpg_error_t rc;

  if (!pwm)
    return FINISH (GPG_ERR_INV_ARG);
  else if (!pwm->ctx)
    {
      rc = init_handle (pwm);
      if (rc)
        return FINISH (GPG_ERR_INV_ARG);
    }
  else if (pwm->fd != ASSUAN_INVALID_FD)
    return FINISH (GPG_ERR_INV_STATE);

  command_start (pwm);

#ifndef __MINGW32__
  if (!(pwm->opts & OPT_SIGPIPE))
    {
      struct sigaction sa = { 0 };

      sa.sa_handler = SIG_IGN;
      sigaction (SIGPIPE, &sa, NULL);
    }
#endif

#ifdef WITH_GNUTLS
  pwm->tls_error = 0;
#endif
  rc = GPG_ERR_UNSUPPORTED_PROTOCOL;

  if (p && (*p == '/' || *p == '~'))
    rc = connect_uds (pwm, p);
  else if (!p || !strncmp (p, "file://", 7))
    {
      if (p)
	p += 7;
#ifdef DEFAULT_PWMD_SOCKET
      else
	p = DEFAULT_PWMD_SOCKET;
#endif
      rc = connect_uds (pwm, p);
    }
  else if (!strncmp (p, "ssh://", 6) || !strncmp (p, "ssh6://", 7) ||
	   !strncmp (p, "ssh4://", 7))
    {
#ifndef WITH_SSH
      return FINISH (GPG_ERR_NOT_IMPLEMENTED);
#else
      char *host = NULL;
      int port;
      char *username = NULL;

      if (!strncmp (p, "ssh6://", 7))
	{
	  pwm->prot = PWMD_IPV6;
	  p += 7;
	}
      else if (!strncmp (p, "ssh4://", 7))
	{
	  pwm->prot = PWMD_IPV4;
	  p += 7;
	}
      else
	{
	  pwm->prot = PWMD_IP_ANY;
	  p += 6;
	}

      rc = _parse_ssh_url (p, &host, &port, &username);
      if (!rc)
	{
	  va_list ap;
	  char *identity = NULL;
	  char *knownhosts = NULL;

	  va_start (ap, url);
	  identity = va_arg (ap, char *);

	  if (!identity && pwm->use_agent != 1)
	    rc = GPG_ERR_INV_ARG;
	  else
	    knownhosts = va_arg (ap, char *);

	  va_end (ap);

	  if (!rc)
	    rc = _do_ssh_connect (pwm, host, port, identity, username,
				  knownhosts);
	  if (!rc)
	    {
	      rc = _connect_finalize (pwm);
	      if (rc)
                free_tcp (pwm);
	    }
	}

      pwmd_free (host);
      pwmd_free (username);
      pwm->local_pinentry = 1;
      pwmd_free (pwm->ssh_passphrase);
      pwm->ssh_passphrase = NULL;
#endif
    }
  else if (!strncmp (p, "tls://", 6) || !strncmp (p, "tls6://", 7) ||
	   !strncmp (p, "tls4://", 7))
    {
#ifndef WITH_GNUTLS
      return FINISH (GPG_ERR_NOT_IMPLEMENTED);
#else
      char *host = NULL;
      int port;

      if (!strncmp (p, "tls6://", 7))
	{
	  pwm->prot = PWMD_IPV6;
	  p += 7;
	}
      else if (!strncmp (p, "tls4://", 7))
	{
	  pwm->prot = PWMD_IPV4;
	  p += 7;
	}
      else
	{
	  pwm->prot = PWMD_IP_ANY;
	  p += 6;
	}

      rc = tls_parse_url (p, &host, &port);
      if (!rc)
	{
	  va_list ap;
	  char *clientcert = NULL;
	  char *clientkey = NULL;
	  char *cacert = NULL;
	  char *server_fp = NULL;

	  va_start (ap, url);
	  clientcert = va_arg (ap, char *);

	  if (!clientcert)
	    rc = GPG_ERR_INV_ARG;
	  else
	    {
	      clientkey = va_arg (ap, char *);
	      if (!clientkey)
		rc = GPG_ERR_INV_ARG;
	      else
		{
		  cacert = va_arg (ap, char *);
		  if (!cacert)
		    rc = GPG_ERR_INV_ARG;
		  else
                    server_fp = va_arg (ap, char *);
		}
	    }

	  va_end (ap);

	  if (!rc)
	    rc = tls_connect (pwm, host, port, clientcert, clientkey, cacert,
                              pwm->tls_priority, server_fp, pwm->tls_verify);
	  if (!rc)
	    {
	      rc = _connect_finalize (pwm);
	      if (rc)
                free_tcp (pwm);
	    }
	}

      pwmd_free (host);
      pwm->local_pinentry = 1;
#endif
    }

  if (!rc)
    pwm->connected = 1;

  return FINISH (rc);
}

static void
disconnect (pwm_t * pwm)
{
  if (!pwm)
    return;

  if (pwm->ctx)
    assuan_release (pwm->ctx);

#if defined(WITH_SSH) || defined(WITH_GNUTLS)
  free_tcp (pwm);
#endif
  pwm->ctx = NULL;
  pwm->fd = ASSUAN_INVALID_FD;
  pwm->connected = 0;
  pwm->user_fd = -1;
}

void
pwmd_close (pwm_t * pwm)
{
  if (!pwm)
    return;

  disconnect (pwm);
  pwmd_free (pwm->pinentry_error);
  pwmd_free (pwm->pinentry_desc);
  pwmd_free (pwm->pinentry_prompt);
  pwmd_free (pwm->pinentry_tty);
  pwmd_free (pwm->pinentry_display);
  pwmd_free (pwm->pinentry_term);
  pwmd_free (pwm->pinentry_lcctype);
  pwmd_free (pwm->pinentry_lcmessages);
  pwmd_free (pwm->filename);
  pwmd_free (pwm->name);
  pwmd_free (pwm->passphrase_info);
  pwmd_free (pwm->passphrase_hint);
  pwmd_free (pwm->ssh_passphrase);
  pwmd_free (pwm->tls_priority);

#ifdef WITH_PINENTRY
  if (pwm->pctx)
    _pinentry_disconnect (pwm);
#endif

  pwmd_free (pwm);
}

static gpg_error_t
inquire_realloc_cb (void *data, const void *buffer, size_t len)
{
  membuf_t *mem = (membuf_t *) data;
  void *p;
  gpg_error_t rc = 0;

  if (!buffer)
    return 0;

  if ((p = pwmd_realloc (mem->buf, mem->len + len)) == NULL)
    return gpg_error (GPG_ERR_ENOMEM);

  mem->buf = p;
  memcpy ((char *) mem->buf + mem->len, buffer, len);
  mem->len += len;

  if (mem->pwm->version >= 0x030101 && len > 1 && mem->pwm->status_func)
    {
      char buf[ASSUAN_LINELENGTH];

      snprintf (buf, sizeof (buf), "XFER %zu %zu", mem->len,
                mem->pwm->data_total);
      rc = mem->pwm->status_func (mem->pwm->status_data, buf);
    }

  return rc;
}

static gpg_error_t
get_password (pwm_t * pwm, char **result, size_t * len,
	      pwmd_pinentry_t w, int echo)
{
  char buf[ASSUAN_LINELENGTH+1] = { 0 }, *p;
#ifndef __MINGW32__
  struct termios told, tnew;
#endif
  char *key = NULL;

  if (result)
    *result = NULL;

  if (len)
    *len = 0;

  if (!isatty (STDIN_FILENO))
    {
      fprintf (stderr, N_("Input is not from a terminal! Failing.\n"));
      return GPG_ERR_ENOTTY;
    }

#ifndef __MINGW32__
  if (!echo)
    {
      if (tcgetattr (STDIN_FILENO, &told) == -1)
	return gpg_error_from_syserror ();

      memcpy (&tnew, &told, sizeof (struct termios));
      tnew.c_lflag &= ~(ECHO);
      tnew.c_lflag |= ICANON | ECHONL;

      if (tcsetattr (STDIN_FILENO, TCSANOW, &tnew) == -1)
	{
	  int n = errno;

	  tcsetattr (STDIN_FILENO, TCSANOW, &told);
	  return gpg_error_from_errno (n);
	}
    }
#endif

  if (pwm->passphrase_hint)
    fprintf(stderr, N_("Key info: %s\n"), pwm->passphrase_hint);

  switch (w)
    {
    case PWMD_PINENTRY_OPEN:
      fprintf (stderr, N_("Passphrase for %s: "), pwm->filename);
      break;
    case PWMD_PINENTRY_OPEN_FAILED:
      fprintf (stderr, N_("Invalid passphrase. Passphrase for %s: "),
	       pwm->filename);
      break;
    case PWMD_PINENTRY_SAVE:
      fprintf (stderr, N_("New passphrase for %s: "), pwm->filename);
      break;
    case PWMD_PINENTRY_SAVE_CONFIRM:
      fprintf (stderr, N_("Repeat passphrase: "));
      break;
    case PWMD_PINENTRY_CONFIRM:
      if (pwm->pinentry_desc)
        fprintf (stderr, "%s", pwm->pinentry_desc);

      if (pwm->pinentry_prompt)
        fprintf (stderr, "%s", pwm->pinentry_prompt);
      else
        fprintf(stderr, N_("Confirm [y/N]:"));
    default:
      break;
    }

  p = fgets (buf, sizeof (buf), stdin);

#ifndef __MINGW32__
  if (!echo)
    tcsetattr (STDIN_FILENO, TCSANOW, &told);
#endif

  if (!p || feof (stdin))
    {
      clearerr (stdin);
      return GPG_ERR_CANCELED;
    }

  /* Strip the newline character. */
  p[strlen (p) - 1] = 0;

  if (buf[0])
    {
      if (w == PWMD_PINENTRY_CONFIRM)
        {
          if (*p != 'y' && *p != 'Y')
            return GPG_ERR_CANCELED;
          return 0;
        }

      key = pwmd_strdup_printf ("%s", p);
      wipememory (buf, 0, sizeof (buf));
      if (!key)
	return GPG_ERR_ENOMEM;

      if (result)
	*result = key;

      if (len)
	*len = strlen (key);
    }
  else
    {
      if (w == PWMD_PINENTRY_CONFIRM)
        return GPG_ERR_CANCELED;

      /* To satisfy inquire_cb(). */
      if (result)
	*result = pwmd_strdup ("");

      if (len)
	*len = 1;
    }

  return 0;
}

gpg_error_t
pwmd_password (pwm_t * pwm, const char *keyword, char **data, size_t * size)
{
  gpg_error_t rc;
  int new_password = 0;
  char *password = NULL, *newpass = NULL;
  int error = 0;

  command_start (pwm);

  if (data)
    *data = NULL;

  if (size)
    *size = 0;

  if (!strcmp (keyword, "NEW_PASSPHRASE"))
    new_password = 1;

  if (!new_password && pwm->pinentry_try)
    error = 1;

again:
  if (pwm->disable_pinentry)
    {
      rc = get_password (pwm, &password, size,
			 new_password ? PWMD_PINENTRY_SAVE :
			 error ? PWMD_PINENTRY_OPEN_FAILED :
			 PWMD_PINENTRY_OPEN, 0);
      if (!rc && new_password)
	rc = get_password (pwm, &newpass, size, PWMD_PINENTRY_SAVE_CONFIRM,
			   0);
    }
  else
    {
      pwmd_pinentry_t which;

      if (error)
	which = new_password
	  ? PWMD_PINENTRY_SAVE_FAILED : PWMD_PINENTRY_OPEN_FAILED;
      else
	which = new_password ? PWMD_PINENTRY_SAVE : PWMD_PINENTRY_OPEN;

      rc = pwmd_getpin (pwm, pwm->filename, &password, size, which);
      if (!rc && new_password && !pwm->pin_repeated)
	rc = pwmd_getpin (pwm, pwm->filename, &newpass, size,
			  PWMD_PINENTRY_SAVE_CONFIRM);
    }

  if (!rc && new_password)
    {
      if (!pwm->pin_repeated && ((!password && newpass)
          || (!newpass && password) || (newpass && password &&
                                        strcmp (newpass, password))))
	{
	  if (pwm->disable_pinentry)
	    fprintf (stderr, N_("Passphrases do not match.\n"));

	  pwmd_free (password);
	  pwmd_free (newpass);
	  password = newpass = NULL;
	  error = 1;
	  goto again;
	}
    }

  (void) pwmd_getpin (pwm, pwm->filename, NULL, NULL, PWMD_PINENTRY_CLOSE);
  pwmd_free (newpass);
  if (!rc && data)
    *data = password;
  else
    pwmd_free (password);

  return rc;
}

static gpg_error_t
inquire_cb (void *data, const char *keyword)
{
  pwm_t *pwm = (pwm_t *) data;
  gpg_error_t rc = 0;
  int free_result = 0;
  char *result = NULL;
  int is_password = 0;
  int new_password = 0;

  if (!strcmp (keyword, "PASSPHRASE") || !strcmp (keyword, "SIGN_PASSPHRASE"))
    is_password = 1;
  else if (!strcmp (keyword, "NEW_PASSPHRASE") || !strcmp (keyword, "GENKEY"))
    new_password = 1;

  /* Shouldn't get this far without a callback. */
  if (!pwm->override_inquire && !pwm->inquire_func
      && !is_password && !new_password)
    return gpg_error (GPG_ERR_ASS_NO_INQUIRE_CB);

  for (;;)
    {
      size_t len = 0;
      gpg_error_t arc;

      result = NULL;

      if (!pwm->override_inquire && (is_password || new_password))
	{
	  free_result = 1;
	  rc = pwmd_password (data, keyword, &result, &len);
	  if (!rc)
	    rc = GPG_ERR_EOF;
	}
      else
	rc = pwm->inquire_func (pwm->inquire_data, keyword, rc, &result,
				&len);

      /* gpg will truncate a passphrase at the first nil byte which may be bad
       * for generated key files. */
      if ((!rc || gpg_err_code (rc) == GPG_ERR_EOF)
          && (is_password || new_password))
        {
          if (len && result && *result)
            {
              for (size_t n = 0; n < len; n++)
                {
                  if (result[n] == 0 && n+1 != len)
                    rc = GPG_ERR_INV_PASSPHRASE;
                }
            }
        }

    cancel:
      if (rc && gpg_err_code (rc) != GPG_ERR_EOF)
	{
#ifndef LIBASSUAN_2_1_0
	  gpg_error_t trc = rc;

	  /* Cancel this inquire. */
	  rc = assuan_send_data (pwm->ctx, NULL, 1);
	  if (!rc)
	    {
	      char *line;
	      size_t len;

	      /* There is a bug (or feature?) in assuan_send_data() that
	       * when cancelling an inquire the next read from the server is
	       * not done until the next command making the next command
	       * fail with GPG_ERR_ASS_UNEXPECTED_CMD.
	       */
	      rc = assuan_read_line (pwm->ctx, &line, &len);

	      /* Restore the original error. This differs from the error
	       * returned from the pwmd command (GPG_ERR_CANCELED). This
	       * error is returned to the calling function.
	       */
	      if (!rc)
		rc = trc;
	    }
#endif
	  break;
	}

      if (gpg_err_code (rc) == GPG_ERR_EOF || !rc)
	{
	  if (len <= 0 && !result)
	    {
	      rc = 0;
	      break;
	    }
	  else if ((len <= 0 && result) || (len && !result))
	    {
	      rc = gpg_error (GPG_ERR_INV_ARG);
	      break;
	    }

	  if (pwm->inquire_maxlen
              && pwm->inquire_sent + len > pwm->inquire_maxlen)
	    {
	      rc = gpg_error (GPG_ERR_TOO_LARGE);
	      if (!free_result)
		rc = pwm->inquire_func (pwm->inquire_data, keyword, rc,
					&result, &len);
	      goto cancel;
	    }

	  arc = assuan_send_data (pwm->ctx, result, len);
	  if (gpg_err_code (rc) == GPG_ERR_EOF)
	    {
	      rc = arc;
	      break;
	    }

	  rc = arc;
	}
      else if (rc)
	break;

      if (!rc)
	{
	  pwm->inquire_sent += len;

	  if (pwm->status_func)
	    {
	      char buf[ASSUAN_LINELENGTH];

	      snprintf (buf, sizeof (buf), "XFER %zu %zu", pwm->inquire_sent,
			pwm->inquire_total);
	      rc = pwm->status_func (pwm->status_data, buf);
	      if (rc)
		continue;
	    }
	}
    }

  if (free_result)
    pwmd_free (result);

  pwm->inquire_maxlen = pwm->inquire_sent = 0;
  return rc;
}

static gpg_error_t
parse_assuan_line (pwm_t * pwm)
{
  gpg_error_t rc;
  char *line;
  size_t len;

  rc = assuan_read_line (pwm->ctx, &line, &len);
  if (!rc)
    {
      if (line[0] == 'O' && line[1] == 'K' &&
	  (line[2] == 0 || line[2] == ' '))
	{
	}
      else if (line[0] == '#')
	{
	}
      else if (line[0] == 'S' && (line[1] == 0 || line[1] == ' '))
	{
          rc = status_cb (pwm, line[1] == 0 ? line + 1 : line + 2);
	}
      else if (line[0] == 'E' && line[1] == 'R' && line[2] == 'R' &&
	       (line[3] == 0 || line[3] == ' '))
	{
	  line += 4;
	  rc = strtol (line, NULL, 10);
	}
    }

  return rc;
}

static void
reset_handle (pwm_t *pwm)
{
  pwm->fd = ASSUAN_INVALID_FD;
  pwm->user_fd = -1;
  pwm->cancel = 0;
  pwm->pinentry_disabled = 0;
  pwm->lock_timeout = -2;
#ifdef WITH_PINENTRY
  if (pwm->pctx)
    _pinentry_disconnect (pwm);
#endif
#if defined(WITH_SSH) || defined(WITH_GNUTLS)
#ifdef WITH_GNUTLS
  pwm->tls_error = 0;
#endif

  if (pwm->tcp)
    {
      pwm->tcp->rc = 0;
    }
#endif
}

gpg_error_t
pwmd_disconnect (pwm_t * pwm)
{
  if (!pwm)
    return FINISH (GPG_ERR_INV_ARG);

  command_start (pwm);

  if (pwm->fd == ASSUAN_INVALID_FD)
    return FINISH (GPG_ERR_INV_STATE);

  disconnect (pwm);
  reset_handle (pwm);
  return 0;
}

/* Note that this should only be called when not in a command. */
gpg_error_t
pwmd_process (pwm_t * pwm)
{
  gpg_error_t rc = 0;
  fd_set fds;
  struct timeval tv = { 0, 0 };
  int n;

  if (!pwm || pwm->fd == ASSUAN_INVALID_FD)
    return FINISH (GPG_ERR_INV_ARG);
  else if (!pwm->ctx)
    return FINISH (GPG_ERR_INV_STATE);

  FD_ZERO (&fds);
#ifdef __MINGW32__
  FD_SET (_get_osfhandle (pwm->fh), &fds);
#else
  FD_SET (pwm->fd, &fds);
#endif
  n = select (HANDLE2SOCKET (pwm->fd) + 1, &fds, NULL, NULL, &tv);

  if (n == -1)
    {
#ifdef _MINGW32__
      switch (WSAGetLastError ())
        {
        case WSAENOTSOCK:
          gpg_err_set_errno (EBADF);
          break;
        case WSAEWOULDBLOCK:
          gpg_err_set_errno (EAGAIN);
          break;
        case WSAECONNRESET:
        case ERROR_BROKEN_PIPE:
          gpg_err_set_errno (EPIPE);
          break;
        default:
          gpg_err_set_errno (EIO);
          break;
        }
#endif
      return FINISH (gpg_error_from_syserror ());
    }

  if (n > 0)
    {
      if (FD_ISSET (HANDLE2SOCKET (pwm->fd), &fds))
        rc = parse_assuan_line (pwm);
    }

  while (!rc && assuan_pending_line (pwm->ctx))
    rc = parse_assuan_line (pwm);

#if defined (WITH_SSH) || defined (WITH_GNUTLS)
  if (gpg_err_code (rc) == GPG_ERR_EOF && pwm->tcp)
    {
      __assuan_close (pwm->ctx, pwm->fd);
      pwm->fd = ASSUAN_INVALID_FD;
    }
#endif

  return FINISH (rc);
}

static gpg_error_t
status_cb (void *data, const char *line)
{
  pwm_t *pwm = data;

  if (!strncmp (line, "INQUIRE_MAXLEN ", 15))
    pwm->inquire_maxlen = strtol (line + 15, NULL, 10);
  else if (!strncmp (line, "BULK ", 5))
    {
      pwmd_free (pwm->bulk_id);
      pwm->bulk_id = NULL;
      if (!strncmp (line+5, "BEGIN ", 6))
        {
          pwm->bulk_id = pwmd_strdup (line+11);
          if (!pwm->bulk_id)
            return gpg_error (GPG_ERR_ENOMEM);
        }
      else if (strncmp (line+5, "END ", 4))
        return gpg_error (GPG_ERR_UNEXPECTED_MSG);
    }
  else if (!strncmp (line, "PASSPHRASE_HINT ", 16))
    {
      pwmd_free (pwm->passphrase_hint);
      pwm->passphrase_hint = pwmd_strdup (line+16);
    }
  else if (!strncmp (line, "PASSPHRASE_INFO ", 16))
    {
      pwmd_free (pwm->passphrase_info);
      pwm->passphrase_info = pwmd_strdup (line+16);
    }
  else if (!strcmp (line, "PIN_REPEATED"))
    pwm->pin_repeated = 1;
  else if (pwm->version >= 0x030101 && pwm->status_func
           && !strncmp (line, "XFER ", 5))
    {
      /* Initial XFER sent from pwmd. Set's the total length of the data to be
       * received. */
      char *p = strchr (line, ' ');
      size_t a = strtoul (++p, NULL, 10);
      size_t b;
      char buf[ASSUAN_LINELENGTH];

      (void)a;

      while (isdigit (*p))
        p++;

      b = strtoul (++p, NULL, 10);
      pwm->data_total = b;
      snprintf (buf, sizeof (buf), "XFER %zu %zu", (size_t)0, pwm->data_total);
      return pwm->status_func (pwm->status_data, buf);
    }
#ifdef WITH_GNUTLS
  else if (!strcmp (line, "REHANDSHAKE"))
    {
      char buf[32];
      ssize_t ret;

      ret = tls_read_hook (pwm, pwm->fd, buf, sizeof (buf));
      if (ret < 0)
        {
          pwm->tls_error = ret;
          return GPG_ERR_GENERAL;
        }
    }
#endif

  if (pwm->status_func)
    return pwm->status_func (pwm->status_data, line);

  return 0;
}

gpg_error_t
_assuan_command (pwm_t * pwm, assuan_context_t ctx,
		 char **result, size_t * len, const char *cmd)
{
  membuf_t data;
  gpg_error_t rc;

  if (!cmd || !*cmd)
    return FINISH (GPG_ERR_INV_ARG);

  if (strlen (cmd) >= ASSUAN_LINELENGTH + 1)
    return FINISH (GPG_ERR_LINE_TOO_LONG);

  data.pwm = pwm;
  data.len = 0;
  data.buf = NULL;

  rc = assuan_transact (ctx, cmd, inquire_realloc_cb, &data,
#if WITH_QUALITY && WITH_PINENTRY
                        pwm->pctx == ctx ? pwm->_inquire_func : inquire_cb,
                        pwm->pctx == ctx ? pwm->_inquire_data : pwm,
#else
                        inquire_cb, pwm,
#endif
                        status_cb, pwm);

  if (rc)
    {
      if (data.buf)
	{
	  pwmd_free (data.buf);
	  data.buf = NULL;
	}
    }
  else
    {
      if (data.buf)
	{
	  inquire_realloc_cb (&data, "", 1);

	  if (result)
	    *result = (char *) data.buf;
	  else
	    pwmd_free (data.buf);

	  if (len)
	    *len = data.len;
	}
    }

  pwm->inquire_maxlen = 0;
  return rc;
}

gpg_error_t
pwmd_command_ap (pwm_t * pwm, char **result, size_t * rlen,
		 pwmd_inquire_cb_t func, void *user, const char *cmd,
		 va_list ap)
{
  char *buf;
  size_t len;
  va_list ap2;

  if (!pwm)
    return GPG_ERR_INV_ARG;

  command_start (pwm);

  if (result)
    *result = NULL;

  if (rlen)
    *rlen = 0;

  if (!pwm || !cmd)
    return FINISH (GPG_ERR_INV_ARG);
  if (!pwm->ctx)
    return FINISH (GPG_ERR_INV_STATE);

  /*
   * C99 allows the dst pointer to be null which will calculate the length
   * of the would-be result and return it.
   */
  va_copy (ap2, ap);
  len = vsnprintf (NULL, 0, cmd, ap);
  buf = (char *) pwmd_malloc (len+1);
  if (!buf)
    {
      va_end (ap2);
      return FINISH (GPG_ERR_ENOMEM);
    }

  if (vsnprintf (buf, len+1, cmd, ap2) != len)
    {
      pwmd_free (buf);
      va_end (ap2);
      return FINISH (GPG_ERR_ENOMEM);
    }

  va_end (ap2);
  if (buf[strlen (buf) - 1] == '\n')
    buf[strlen (buf) - 1] = 0;
  if (buf[strlen (buf) - 1] == '\r')
    buf[strlen (buf) - 1] = 0;

  pwm->inquire_func = func;
  pwm->inquire_data = user;
  pwm->inquire_sent = 0;
  gpg_error_t rc = _assuan_command (pwm, pwm->ctx, result, rlen, buf);
  pwmd_free (buf);
  return rc;
}

gpg_error_t
pwmd_command (pwm_t * pwm, char **result, size_t * len,
	      pwmd_inquire_cb_t func, void *user, const char *cmd, ...)
{
  va_list ap;

  if (result)
    *result = NULL;

  if (len)
    *len = 0;

  if (!pwm || !cmd)
    return FINISH (GPG_ERR_INV_ARG);
  if (!pwm->ctx)
    return FINISH (GPG_ERR_INV_STATE);

  va_start (ap, cmd);
  gpg_error_t rc = pwmd_command_ap (pwm, result, len, func, user, cmd, ap);
  va_end (ap);
  return rc;
}

gpg_error_t
pwmd_bulk (pwm_t *pwm, char **result, size_t *size, pwmd_inquire_cb_t func,
           void *user, const char *cmds, size_t len)
{
  gpg_error_t rc;
  struct bulk_inquire_s *inq;

  if (!pwm)
    return GPG_ERR_INV_ARG;

  if (!pwm->ctx)
    return FINISH (GPG_ERR_INV_STATE);

  if (result)
    *result = NULL;

  if (size)
    *size = 0;

  inq = pwmd_calloc (1, sizeof (struct bulk_inquire_s));
  if (!inq)
    return FINISH (GPG_ERR_ENOMEM);

  command_start (pwm);
  inq->pwm = pwm;
  inq->user = user;
  inq->func = func;
  inq->data = cmds;
  inq->size = len;
  pwm->inquire_func = bulk_inquire_cb;
  pwm->inquire_data = inq;
  pwm->inquire_sent = 0;
  if (len +7 >= ASSUAN_LINELENGTH)
    rc = _assuan_command (pwm, pwm->ctx, result, size, "BULK --inquire");
  else
    {
      char *buf = pwmd_strdup_printf ("BULK %s", cmds);

      if (buf)
        rc = _assuan_command (pwm, pwm->ctx, result, size, buf);
      else
        rc = GPG_ERR_ENOMEM;

      pwmd_free (buf);
    }

  pwmd_free (pwm->bulk_id);
  pwm->bulk_id = NULL;
  pwmd_free (inq);
  return FINISH (rc);
}

static gpg_error_t
set_pinentry_options (pwm_t * pwm, int which, char **cmds, size_t *offset)
{
  gpg_error_t rc;
  char *str = NULL;
  gpg_error_t rcs[] = { 0, GPG_ERR_MISSING_ERRNO };

  rc = pwmd_bulk_append_rc (cmds, rcs, "PIN_ENABLE", strlen ("PIN_ENABLE"),
                            "OPTION", "disable-pinentry=0", 18, offset);

  if (!rc && pwm->pinentry_tty)
    {
      str = pwmd_strdup_printf ("TTYNAME=%s", pwm->pinentry_tty);
      (*offset)--;
      if (str)
        rc = pwmd_bulk_append_rc (cmds, rcs, "PIN_TTY", strlen ("PIN_TTY"),
                                  "OPTION", str, strlen (str), offset);
      else
        rc = GPG_ERR_ENOMEM;

      pwmd_free (str);
    }

  if (!rc && pwm->pinentry_term)
    {
      str = pwmd_strdup_printf ("TTYTYPE=%s", pwm->pinentry_term);
      (*offset)--;
      if (str)
        rc = pwmd_bulk_append_rc (cmds, rcs, "PIN_TERM", strlen ("PIN_TERM"),
                               "OPTION", str, strlen (str), offset);
      else
        rc = GPG_ERR_ENOMEM;

      pwmd_free (str);
    }

  if (!rc && pwm->pinentry_display)
    {
      str = pwmd_strdup_printf ("DISPLAY=%s", pwm->pinentry_display);
      (*offset)--;
      if (str)
        rc = pwmd_bulk_append_rc (cmds, rcs, "PIN_DISPLAY",
                                  strlen ("PIN_DISPLAY"), "OPTION", str,
                                  strlen (str), offset);
      else
        rc = GPG_ERR_ENOMEM;

      pwmd_free (str);
    }

  if (!rc && pwm->pinentry_desc)
    {
      str = pwmd_strdup_printf ("DESC=%s", pwm->pinentry_desc);
      (*offset)--;
      if (str)
        rc = pwmd_bulk_append_rc (cmds, rcs, "PIN_DESC", strlen ("PIN_DESC"),
                                  "OPTION", str, strlen (str), offset);
      else
        rc = GPG_ERR_ENOMEM;

      pwmd_free (str);
    }

  if (!rc && pwm->pinentry_lcctype)
    {
      str = pwmd_strdup_printf ("LC_CTYPE=%s", pwm->pinentry_lcctype);
      (*offset)--;
      if (str)
        rc = pwmd_bulk_append_rc (cmds, rcs, "PIN_LCCTYPE",
                                  strlen ("PIN_LCCTYPE"), "OPTION", str,
                                  strlen (str), offset);
      else
        rc = GPG_ERR_ENOMEM;

      pwmd_free (str);
    }

  if (!rc && pwm->pinentry_lcmessages)
    {
      str = pwmd_strdup_printf ("LC_MESSAGES=%s", pwm->pinentry_lcmessages);
      (*offset)--;
      if (str)
        rc = pwmd_bulk_append_rc (cmds, rcs, "PIN_LCMESSAGES",
                                  strlen ("PIN_LCMESSAGES"), "OPTION", str,
                                  strlen (str), offset);
      else
        rc = GPG_ERR_ENOMEM;

      pwmd_free (str);
    }

  if (!rc && ((pwm->pinentry_timeout >= 0
              && pwm->pinentry_timeout != pwm->current_pinentry_timeout)
      || (pwm->pinentry_timeout == -1 && pwm->pinentry_timeout !=
          pwm->current_pinentry_timeout)))
    {
      str = pwmd_strdup_printf ("pinentry-timeout=%i", pwm->pinentry_timeout);
      (*offset)--;
      if (str)
        rc = pwmd_bulk_append_rc (cmds, rcs, "PIN_TIMEOUT",
                                  strlen ("PIN_TIMEOUT"), "OPTION", str,
                                  strlen (str), offset);
      else
        rc = GPG_ERR_ENOMEM;

      pwmd_free (str);
      if (!rc)
        pwm->current_pinentry_timeout = pwm->pinentry_timeout;
    }

  return rc;
}

gpg_error_t
pwmd_socket_type (pwm_t * pwm, pwmd_socket_t * result)
{
  if (!pwm || !result)
    return FINISH (GPG_ERR_INV_ARG);

  *result = PWMD_SOCKET_LOCAL;

  if (pwm->fd == ASSUAN_INVALID_FD)
    return FINISH (GPG_ERR_INV_STATE);

  if (pwm->user_fd != -1)
    {
      *result = PWMD_SOCKET_USER;
      return 0;
    }

#if defined(WITH_SSH) || defined(WITH_GNUTLS)
#ifdef WITH_SSH
  if (pwm->tcp && pwm->tcp->ssh)
    *result = PWMD_SOCKET_SSH;
#endif
#ifdef WITH_GNUTLS
  if (pwm->tcp && pwm->tcp->tls)
    *result = PWMD_SOCKET_TLS;
#endif
#endif
  return 0;
}

static gpg_error_t
disable_pinentry (pwm_t *pwm, int *disable, char **cmds, size_t *offset,
                  int force)
{
  gpg_error_t rc;
#if defined(WITH_SSH) || defined(WITH_GNUTLS)
  int no_pinentry = pwm->disable_pinentry || pwm->tcp || pwm->local_pinentry;
#else
  int no_pinentry = pwm->disable_pinentry || pwm->local_pinentry;
#endif
  char *buf;
  gpg_error_t rcs[] = { 0, GPG_ERR_MISSING_ERRNO };

  if (force != -1)
    no_pinentry = !!force;

  if (disable)
    *disable = no_pinentry;

  if (force == -1 && pwm->pinentry_disabled && no_pinentry)
    return 0;
  else if (force == -1 && !pwm->pinentry_disabled && !no_pinentry)
    return 0;

  buf = pwmd_strdup_printf ("disable-pinentry=%i", no_pinentry);
  if (buf && force == -1)
    rc = pwmd_bulk_append_rc (cmds, rcs, "PIN_ENABLE", strlen ("PIN_ENABLE"),
                              "OPTION", buf, strlen (buf), offset);
  else if (buf)
    rc = pwmd_bulk_append (cmds, "PIN_ENABLE", strlen ("PIN_ENABLE"),
                              "OPTION", buf, strlen (buf), NULL);
  else
    rc = GPG_ERR_ENOMEM;

  pwmd_free (buf);
  return rc;
}

static gpg_error_t
parse_pinentry_results (pwm_t *pwm, const char *result, size_t size,
                        size_t *offset, int no_pinentry)
{
  gpg_error_t rc, rrc = 0;
  size_t rsize;
  const char *r;

  rc = pwmd_bulk_result (result, size, "PIN_ENABLE", 10, offset, &r, &rsize,
                         &rrc);
  if (!rc && !rrc)
    pwm->pinentry_disabled = no_pinentry;

  if (!rrc && (!rc || gpg_err_code (rc) == GPG_ERR_NOT_FOUND))
    rc = pwmd_bulk_result (result, size, "PIN_TTY", 7, offset, &r, &rsize,
                           &rrc);

  if (!rrc && (!rc || gpg_err_code (rc) == GPG_ERR_NOT_FOUND))
    rc = pwmd_bulk_result (result, size, "PIN_TERM", 8, offset, &r, &rsize,
                           &rrc);

  if (!rrc && (!rc || gpg_err_code (rc) == GPG_ERR_NOT_FOUND))
    rc = pwmd_bulk_result (result, size, "PIN_DISPLAY", 11, offset, &r, &rsize,
                           &rrc);

  if (!rrc && (!rc || gpg_err_code (rc) == GPG_ERR_NOT_FOUND))
    rc = pwmd_bulk_result (result, size, "PIN_DESC", 8, offset, &r, &rsize,
                           &rrc);

  if (!rrc && (!rc || gpg_err_code (rc) == GPG_ERR_NOT_FOUND))
    rc = pwmd_bulk_result (result, size, "PIN_LCCTYPE", 11, offset, &r, &rsize,
                           &rrc);

  if (!rrc && (!rc || gpg_err_code (rc) == GPG_ERR_NOT_FOUND))
    rc = pwmd_bulk_result (result, size, "PIN_LCMESSAGES", 14, offset, &r,
                           &rsize, &rrc);

  if (!rrc && (!rc || gpg_err_code (rc) == GPG_ERR_NOT_FOUND))
    rc = pwmd_bulk_result (result, size, "PIN_TIMEOUT", 11, offset, &r, &rsize,
                           &rrc);

  if (gpg_err_code (rc) == GPG_ERR_NOT_FOUND)
    rc = 0;

  return rc ? rc : rrc;
}

/* It would be quicker if we could add OPEN to the bulk command list, but since
   it may use an inquire from the server to obtain the passphrase, the OPEN
   must be done separately from the pinentry and lock timeout commands. */
gpg_error_t
pwmd_open (pwm_t * pwm, const char *filename, pwmd_inquire_cb_t cb,
	   void *data)
{
  gpg_error_t rc = 0, trc = 0;
  int no_pinentry = 0;
  char *cmds = NULL;
  size_t offset = 0, toffset;
  gpg_error_t rcs[] = { 0, GPG_ERR_MISSING_ERRNO };
  char *result;
  size_t size;
  char *buf;

  if (!pwm || !filename || !*filename)
    return FINISH (GPG_ERR_INV_ARG);

  if (!pwm->ctx)
    return FINISH (GPG_ERR_INV_STATE);

  // A dummy command to deal with pwmd_bulk_append_rc().
  rc = pwmd_bulk_append (&cmds, "NOP", 3, "NOP", NULL, 0, &offset);
  if (rc)
    return FINISH (rc);

  toffset = offset;
  command_start (pwm);
  rc = disable_pinentry (pwm, &no_pinentry, &cmds, &offset, -1);
  if (!rc && !no_pinentry)
    {
      TEST_BULK_OFFSET (offset, toffset);
      rc = set_pinentry_options (pwm, PWMD_WHICH_NONE, &cmds, &offset);
    }

  if (rc)
    goto done;

  if (pwm->lock_timeout != -2)
    {
      buf = pwmd_strdup_printf ("lock-timeout=%li", pwm->lock_timeout);
      if (buf)
        {
          TEST_BULK_OFFSET (offset, toffset);
          rc = pwmd_bulk_append_rc (&cmds, rcs, "LOCK_TO",
                                    strlen ("LOCK_TO"), "OPTION", buf,
                                    strlen (buf), &offset);
        }
      else
        rc = GPG_ERR_ENOMEM;

      pwmd_free (buf);
    }

  if (!rc)
    rc = pwmd_bulk_finalize (&cmds);

  if (rc)
    goto done;

  // Send pinentry options.
  rc = pwmd_bulk (pwm, &result, &size, cb, data, cmds, strlen (cmds));
  pwmd_free (cmds);
  cmds = NULL;
  if (!rc)
    {
      const char *r;
      gpg_error_t rrc = 0;
      size_t rsize;

      offset = 0;
      rc = parse_pinentry_results (pwm, result, size, &offset,
                                   no_pinentry);

      if (!rc || gpg_err_code (rc) == GPG_ERR_NOT_FOUND)
        rc = pwmd_bulk_result (result, size, "LOCK_TO", 7, &offset, &r,
                               &rsize, &rrc);

      if (gpg_err_code (rc) == GPG_ERR_NOT_FOUND)
        rc = 0;

      if (!rc)
        rc = rrc;

      pwmd_free (result);
    }

  if (rc)
    goto done;

  pwm->pinentry_try = 0;
  pwmd_free (pwm->filename);
  pwm->filename = pwmd_strdup (filename);

  do
    {
      rc = pwmd_command (pwm, NULL, NULL, cb, data, "OPEN %s%s",
                         (pwm->opts & OPT_LOCK_ON_OPEN)
                         ? "--lock " : "", filename);
    } while ((gpg_err_code (rc) == GPG_ERR_BAD_PASSPHRASE
              || gpg_err_code (rc) == GPG_ERR_DECRYPT_FAILED)
             && pwm->pinentry_disabled
             && ++pwm->pinentry_try < pwm->pinentry_tries);

  trc = rc; // Keep the return code from the OPEN command.

  // Try to restore pinentry settings even after OPEN failed.
  offset = 0;
  rc = disable_pinentry (pwm, &no_pinentry, &cmds, &offset, 0);
  if (!rc)
    rc = pwmd_bulk_finalize (&cmds);

  rc = pwmd_bulk (pwm, &result, &size, cb, data, cmds, strlen (cmds));
  pwmd_free (cmds);
  cmds = NULL;
  if (!rc)
    {
      const char *r;
      gpg_error_t rrc = 0;
      size_t rsize;

      offset = 0;
      rc = parse_pinentry_results (pwm, result, size, &offset,
                                   no_pinentry);

      if (!rc || gpg_err_code (rc) == GPG_ERR_NOT_FOUND)
        rc = pwmd_bulk_result (result, size, "LOCK_TO", 7, &offset, &r,
                               &rsize, &rrc);

      if (gpg_err_code (rc) == GPG_ERR_NOT_FOUND)
        rc = 0;

      if (!rc)
        rc = rrc;

      pwmd_free (result);
    }

  pwm->pinentry_try = 0;
  pwm->pinentry_disabled = 0;

  if (trc) // Open failed.
    {
      pwmd_free (pwm->filename);
      pwm->filename = NULL;
    }

done:
  pwmd_free (cmds);
  pwmd_free (pwm->passphrase_hint);
  pwmd_free (pwm->passphrase_info);
  pwm->passphrase_info = pwm->passphrase_hint = NULL;
  return FINISH (trc ? trc : rc);
}

static gpg_error_t
do_pwmd_save_passwd (pwm_t * pwm, const char *args, pwmd_inquire_cb_t cb,
		     void *data, int which)
{
  gpg_error_t rc = 0, trc = 0;
  int no_pinentry = 0;
  char *cmds = NULL;
  size_t offset = 0, toffset;
  char *result;
  size_t size;
  const char *cmd;

  if (!pwm)
    return FINISH (GPG_ERR_INV_ARG);
  if (!pwm->ctx)
    return FINISH (GPG_ERR_INV_STATE);

  // A dummy command to deal with pwmd_bulk_append_rc().
  rc = pwmd_bulk_append (&cmds, "NOP", 3, "NOP", NULL, 0, &offset);
  if (rc)
    return FINISH (rc);

  toffset = offset;
  command_start (pwm);
  rc = disable_pinentry (pwm, &no_pinentry, &cmds, &offset, -1);
  if (!rc && !no_pinentry)
    {
      TEST_BULK_OFFSET (offset, toffset);
      rc = set_pinentry_options (pwm, which, &cmds, &offset);
    }

  if (!rc)
    rc = pwmd_bulk_finalize (&cmds);

  if (rc)
    goto done;

  // Send pinentry options.
  rc = pwmd_bulk (pwm, &result, &size, cb, data, cmds, strlen (cmds));
  pwmd_free (cmds);
  cmds = NULL;
  if (!rc)
    {
      offset = 0;
      rc = parse_pinentry_results (pwm, result, size, &offset,
                                   no_pinentry);
      if (gpg_err_code (rc) == GPG_ERR_NOT_FOUND)
        rc = 0;

      pwmd_free (result);
    }

  if (rc)
    goto done;

  if (which == PWMD_WHICH_SAVE)
    cmd = "SAVE";
  else if (which == PWMD_WHICH_PASSWD)
    cmd = "PASSWD";
  else
    cmd = "GENKEY";

  trc = rc = pwmd_command (pwm, NULL, NULL, cb, data, "%s %s", cmd,
                           args ? args : "");

  // Try to restore pinentry settings even after 'cmd' failed.
  offset = 0;
  rc = disable_pinentry (pwm, &no_pinentry, &cmds, &offset, 0);
  if (!rc)
    rc = pwmd_bulk_finalize (&cmds);

  if (!rc)
    rc = pwmd_bulk (pwm, &result, &size, cb, data, cmds, strlen (cmds));

  if (!rc)
    {
      offset = 0;
      rc = parse_pinentry_results (pwm, result, size, &offset,
                                   no_pinentry);
      pwmd_free (result);
    }

done:
  pwm->disable_pinentry = 0;
  pwmd_free (cmds);
  pwmd_free (pwm->passphrase_hint);
  pwmd_free (pwm->passphrase_info);
  pwm->passphrase_info = pwm->passphrase_hint = NULL;
  return FINISH (trc ? trc : rc);
}

gpg_error_t
pwmd_passwd (pwm_t * pwm, const char *args, pwmd_inquire_cb_t cb, void *data)
{
  return do_pwmd_save_passwd (pwm, args, cb, data, PWMD_WHICH_PASSWD);
}

gpg_error_t
pwmd_save (pwm_t * pwm, const char *args, pwmd_inquire_cb_t cb, void *data)
{
  return do_pwmd_save_passwd (pwm, args, cb, data, PWMD_WHICH_SAVE);
}

gpg_error_t
pwmd_genkey (pwm_t * pwm, const char *args, pwmd_inquire_cb_t cb, void *data)
{
  return do_pwmd_save_passwd (pwm, args, cb, data, PWMD_WHICH_GENKEY);
}

static gpg_error_t
pwmd_get_set_opt (pwm_t *pwm, pwmd_option_t opt, int get, va_list ap)
{
  long l, *longp;
  int n, *intp;
  size_t *sizetp;
  char *arg1, **charpp;
  gpg_error_t rc = 0;

  if (!pwm)
    return GPG_ERR_INV_ARG;

  command_start (pwm);
  switch (opt)
    {
    case PWMD_OPTION_SERVER_VERSION:
      if (!get)
        return GPG_ERR_NOT_SUPPORTED;

      if (!pwm->ctx)
        rc = GPG_ERR_INV_STATE;
      else
        {
          unsigned *u = va_arg (ap, unsigned *);
          *u = pwm->version;
        }

      break;
    case PWMD_OPTION_SIGPIPE:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->opts & OPT_SIGPIPE ? 1 : 0;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0 || n > 1)
        {
          rc = GPG_ERR_INV_VALUE;
          break;
        }

      if (n)
	pwm->opts |= OPT_SIGPIPE;
      else
	pwm->opts &= ~OPT_SIGPIPE;

      break;
    case PWMD_OPTION_STATE_STATUS:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->opts & OPT_STATE_STATUS ? 1 : 0;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0 || n > 1)
        {
          rc = GPG_ERR_INV_VALUE;
          break;
        }

      if (n)
	pwm->opts |= OPT_STATE_STATUS;
      else
	pwm->opts &= ~OPT_STATE_STATUS;

      break;
    case PWMD_OPTION_LOCK_ON_OPEN:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->opts & OPT_LOCK_ON_OPEN ? 1 : 0;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0 || n > 1)
        {
          rc = GPG_ERR_INV_VALUE;
          break;
        }

      if (n)
	pwm->opts |= OPT_LOCK_ON_OPEN;
      else
	pwm->opts &= ~OPT_LOCK_ON_OPEN;

      break;
    case PWMD_OPTION_LOCK_TIMEOUT:
      if (get)
	{
	  longp = va_arg (ap, long *);
	  *longp = pwm->lock_timeout;
	  break;
	}

      l = va_arg (ap, long);
      if (l < -1)
        {
          rc = GPG_ERR_INV_VALUE;
          break;
        }

      pwm->lock_timeout = l;
      break;
    case PWMD_OPTION_INQUIRE_TOTAL:
      if (get)
	{
	  sizetp = va_arg (ap, size_t *);
	  *sizetp = pwm->inquire_total;
	  break;
	}

      pwm->inquire_total = va_arg (ap, size_t);
      break;
    case PWMD_OPTION_STATUS_CB:
      if (get)
	{
	  pwmd_status_cb_t *cb = va_arg (ap, pwmd_status_cb_t *);

	  *cb = pwm->status_func;
	  break;
	}

      pwm->status_func = va_arg (ap, pwmd_status_cb_t);
      break;
    case PWMD_OPTION_STATUS_DATA:
      if (get)
	{
	  void **data = va_arg (ap, void **);

	  *data = pwm->status_data;
	  break;
	}

      pwm->status_data = va_arg (ap, void *);
      break;
    case PWMD_OPTION_NO_PINENTRY:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->disable_pinentry;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0 || n > 1)
	rc = GPG_ERR_INV_VALUE;
      else
	pwm->disable_pinentry = n;
      break;
    case PWMD_OPTION_LOCAL_PINENTRY:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->local_pinentry;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0 || n > 1)
	rc = GPG_ERR_INV_VALUE;
      else
	pwm->local_pinentry = n;

      break;
    case PWMD_OPTION_PINENTRY_TIMEOUT:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->pinentry_timeout;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0)
	rc = GPG_ERR_INV_VALUE;
      else
	pwm->pinentry_timeout = n;
      break;
    case PWMD_OPTION_PINENTRY_TRIES:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->pinentry_tries;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0)
        rc = GPG_ERR_INV_VALUE;
      else
        pwm->pinentry_tries = n;
      break;
    case PWMD_OPTION_PINENTRY_PATH:
      if (get)
	{
	  charpp = va_arg (ap, char **);
	  *charpp = (char *)PINENTRY_PATH;
	  break;
	}
      break;
    case PWMD_OPTION_PINENTRY_TTY:
      if (get)
	{
	  charpp = va_arg (ap, char **);
	  *charpp = pwm->pinentry_tty;
	  break;
	}

      arg1 = va_arg (ap, char *);
      pwmd_free (pwm->pinentry_tty);
      pwm->pinentry_tty = arg1 ? pwmd_strdup (arg1) : NULL;
      break;
    case PWMD_OPTION_PINENTRY_DISPLAY:
      if (get)
	{
	  charpp = va_arg (ap, char **);
	  *charpp = pwm->pinentry_display;
	  break;
	}

      arg1 = va_arg (ap, char *);
      pwmd_free (pwm->pinentry_display);
      pwm->pinentry_display = arg1 && *arg1 ? pwmd_strdup (arg1) : NULL;
#ifndef __MINGW32__
      if (!pwm->pinentry_display)
        unsetenv ("DISPLAY");
#endif
      break;
    case PWMD_OPTION_PINENTRY_TERM:
      if (get)
	{
	  charpp = va_arg (ap, char **);
	  *charpp = pwm->pinentry_term;
	  break;
	}

      arg1 = va_arg (ap, char *);
      pwmd_free (pwm->pinentry_term);
      pwm->pinentry_term = arg1 && *arg1 ? pwmd_strdup (arg1) : NULL;
#ifndef __MINGW32__
      if (pwm->pinentry_term)
        unsetenv ("TERM");
#endif
      break;
    case PWMD_OPTION_PINENTRY_ERROR:
      if (get)
	{
	  charpp = va_arg (ap, char **);
	  *charpp = pwm->pinentry_error;
	  break;
	}

      arg1 = va_arg (ap, char *);
      pwmd_free (pwm->pinentry_error);
      if (pwm->disable_pinentry)
        pwm->pinentry_error = arg1 ? pwmd_strdup (arg1) : NULL;
      else
        pwm->pinentry_error = arg1 ? _percent_escape (arg1) : NULL;
      break;
    case PWMD_OPTION_PINENTRY_PROMPT:
      if (get)
	{
	  charpp = va_arg (ap, char **);
	  *charpp = pwm->pinentry_prompt;
	  break;
	}

      arg1 = va_arg (ap, char *);
      pwmd_free (pwm->pinentry_prompt);
      if (pwm->disable_pinentry)
        pwm->pinentry_prompt = arg1 ? pwmd_strdup (arg1) : NULL;
      else
        pwm->pinentry_prompt = arg1 ? _percent_escape (arg1) : NULL;
      break;
    case PWMD_OPTION_PINENTRY_DESC:
      if (get)
	{
	  charpp = va_arg (ap, char **);
	  *charpp = pwm->pinentry_desc;
	  break;
	}

      arg1 = va_arg (ap, char *);
      pwmd_free (pwm->pinentry_desc);
      if (pwm->disable_pinentry)
        pwm->pinentry_desc = arg1 ? pwmd_strdup (arg1) : NULL;
      else
        pwm->pinentry_desc = arg1 ? _percent_escape (arg1) : NULL;
      break;
    case PWMD_OPTION_PINENTRY_LC_CTYPE:
      if (get)
	{
	  charpp = va_arg (ap, char **);
	  *charpp = pwm->pinentry_lcctype;
	  break;
	}

      arg1 = va_arg (ap, char *);
      pwmd_free (pwm->pinentry_lcctype);
      pwm->pinentry_lcctype = arg1 ? pwmd_strdup (arg1) : NULL;
      break;
    case PWMD_OPTION_PINENTRY_LC_MESSAGES:
      if (get)
	{
	  charpp = va_arg (ap, char **);
	  *charpp = pwm->pinentry_lcmessages;
	  break;
	}

      arg1 = va_arg (ap, char *);
      pwmd_free (pwm->pinentry_lcmessages);
      pwm->pinentry_lcmessages = arg1 ? pwmd_strdup (arg1) : NULL;
      break;
    case PWMD_OPTION_KNOWNHOST_CB:
      if (get)
	{
	  pwmd_knownhost_cb_t *cb = va_arg (ap, pwmd_knownhost_cb_t *);

	  *cb = pwm->kh_cb;
	  break;
	}

      pwm->kh_cb = va_arg (ap, pwmd_knownhost_cb_t);
      break;
    case PWMD_OPTION_KNOWNHOST_DATA:
      if (get)
	{
	  void **data = va_arg (ap, void **);

	  *data = pwm->kh_data;
	  break;
	}

      pwm->kh_data = va_arg (ap, void *);
      break;
    case PWMD_OPTION_PASSPHRASE_CB:
      if (get)
	{
	  pwmd_passphrase_cb_t *cb = va_arg (ap, pwmd_passphrase_cb_t *);

	  *cb = pwm->passphrase_cb;
	  break;
	}

      pwm->passphrase_cb = va_arg (ap, pwmd_passphrase_cb_t);
      break;
    case PWMD_OPTION_PASSPHRASE_DATA:
      if (get)
	{
	  void **data = va_arg (ap, void **);

	  *data = pwm->passphrase_data;
	  break;
	}

      pwm->passphrase_data = va_arg (ap, void *);
      break;
    case PWMD_OPTION_SSH_AGENT:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->use_agent;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0 || n > 1)
        rc = GPG_ERR_INV_VALUE;
      else
        pwm->use_agent = n;
      break;
    case PWMD_OPTION_SSH_PASSPHRASE:
      if (get)
        return GPG_ERR_NOT_SUPPORTED;

      pwmd_free (pwm->ssh_passphrase);
      pwm->ssh_passphrase = NULL;
      arg1 = va_arg (ap, char *);
      if (arg1)
        {
          pwm->ssh_passphrase = pwmd_strdup (arg1);
          if (!pwm->ssh_passphrase)
            return GPG_ERR_ENOMEM;
        }
      break;
    case PWMD_OPTION_SSH_NEEDS_PASSPHRASE:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->needs_passphrase;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0 || n > 1)
        rc = GPG_ERR_INV_VALUE;
      else
        pwm->needs_passphrase = n;
      break;
    case PWMD_OPTION_TLS_VERIFY:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->tls_verify;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0 || n > 1)
        rc = GPG_ERR_INV_VALUE;
      else
        pwm->tls_verify = n;
      break;
    case PWMD_OPTION_TLS_PRIORITY:
      if (get)
        {
          charpp = va_arg (ap, char **);
          *charpp = pwm->tls_priority;
          break;
        }

      pwmd_free (pwm->tls_priority);
      pwm->tls_priority = NULL;
      arg1 = va_arg (ap, char *);
      if (arg1)
        {
          pwm->tls_priority = pwmd_strdup (arg1);
          if (!pwm->tls_priority)
            rc = GPG_ERR_ENOMEM;
        }
      break;
    case PWMD_OPTION_SOCKET_TIMEOUT:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->socket_timeout;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0)
        {
          rc = GPG_ERR_INV_VALUE;
          break;
        }
      else
        pwm->socket_timeout = n;

#ifdef WITH_SSH
      if (pwm->tcp && pwm->tcp->ssh && pwm->tcp->ssh->session)
	{
	  pwm->tcp->ssh->timeout = pwm->socket_timeout;
	  libssh2_session_set_timeout (pwm->tcp->ssh->session,
				       pwm->socket_timeout * 1000);
	}
#endif
#ifdef WITH_GNUTLS
      if (pwm->tcp && pwm->tcp->tls && pwm->tcp->tls->session)
	pwm->tcp->tls->timeout = pwm->socket_timeout;
#endif
      break;
    case PWMD_OPTION_READ_CB:
      if (get)
        {
	  pwmd_read_cb_t *cb = va_arg (ap, pwmd_read_cb_t *);

	  *cb = pwm->read_cb;
	  break;
        }
      pwm->read_cb = va_arg (ap, pwmd_read_cb_t);
      break;
    case PWMD_OPTION_READ_CB_DATA:
      if (get)
	{
	  void **data = va_arg (ap, void **);

	  *data = pwm->read_cb_data;
	  break;
	}

      pwm->read_cb_data = va_arg (ap, void *);
      break;
    case PWMD_OPTION_WRITE_CB:
      if (get)
        {
	  pwmd_write_cb_t *cb = va_arg (ap, pwmd_write_cb_t *);

	  *cb = pwm->write_cb;
	  break;
        }
      pwm->write_cb = va_arg (ap, pwmd_write_cb_t);
      break;
    case PWMD_OPTION_WRITE_CB_DATA:
      if (get)
	{
	  void **data = va_arg (ap, void **);

	  *data = pwm->write_cb_data;
	  break;
	}

      pwm->write_cb_data = va_arg (ap, void *);
      break;
    case PWMD_OPTION_OVERRIDE_INQUIRE:
      if (get)
	{
	  intp = va_arg (ap, int *);
	  *intp = pwm->override_inquire;
	  break;
	}

      n = va_arg (ap, int);
      if (n < 0 || n > 1)
        rc = GPG_ERR_INV_VALUE;
      else
        pwm->override_inquire = n;
      break;
    default:
      rc = GPG_ERR_UNKNOWN_OPTION;
      break;
    }

  return FINISH (rc);
}

gpg_error_t
pwmd_setopt (pwm_t * pwm, int opt, ...)
{
  va_list ap;
  gpg_error_t rc = 0;

  va_start (ap, opt);
  rc = pwmd_get_set_opt (pwm, opt, 0, ap);
  va_end (ap);
  return FINISH (rc);
}

gpg_error_t
pwmd_getopt (pwm_t *pwm, int opt, ...)
{
  va_list ap;
  gpg_error_t rc = 0;

  va_start (ap, opt);
  rc = pwmd_get_set_opt (pwm, opt, 1, ap);
  va_end (ap);
  return FINISH (rc);
}

gpg_error_t
set_rcdefaults (pwm_t *pwm, char *filename)
{
  char *f;
  FILE *fp;
  char *line = NULL, *p;
  unsigned line_n = 0;
  gpg_error_t rc = 0;

#ifndef __MINGW32__
  if (!filename && isatty (STDIN_FILENO))
    {
      char buf[256];
      int err = ttyname_r (STDOUT_FILENO, buf, sizeof (buf));

      if (!err)
        {
          rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_TTY, buf);
          if (rc)
            return rc;

          if (getenv ("TERM"))
            {
              rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_TERM, getenv ("TERM"));
              if (rc)
                return rc;
            }
        }
    }

  if (!filename && getenv ("DISPLAY"))
    {
      rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_DISPLAY, getenv ("DISPLAY"));
      if (rc)
        return rc;
    }
#endif

#ifdef __MINGW32__
  f = pwmd_strdup ("libpwmd.conf");
#else
  f = _expand_homedir (filename ? filename : (char *)"~/.config/libpwmd.conf",
                       NULL);
#endif
  if (f)
    {
      line = pwmd_malloc (LINE_MAX);
      if (line)
        {
          fp = fopen (f, "r");
          if (!fp)
            {
              pwmd_free (line);
              pwmd_free (f);
              return 0;
            }
        }
      else
        rc = GPG_ERR_ENOMEM;
    }
  else
    rc = GPG_ERR_ENOMEM;

  if (rc)
    {
      pwmd_free (f);
      pwmd_free (line);
      return rc;
    }

  while ((p = fgets (line, LINE_MAX, fp)))
    {
      char name[32] = {0}, val[512] = {0};
      char *np;
      char *t;
      size_t len = 0;

      line_n++;

      while (isspace (*p))
        p++;

      if (!*p || *p == '#')
        continue;

      if (p[strlen (p)-1] == '\n')
        p[strlen (p)-1] = 0;

      t = strchr (p, '=');
      if (!t)
        {
          fprintf(stderr, N_("%s(%u): malformed line\n"), f, line_n);
          continue;
        }

      for (np = name; p != t; p++)
        {
          if (++len == sizeof (name))
            break;

          if (isspace (*p))
            break;

          *np++ = *p;
        }

      while (isspace (*t))
        t++;
      t++; // '='
      while (isspace (*t))
        t++;

      strncpy (val, t, sizeof (val)-1);

      if (!strcasecmp (name, "pinentry-tries"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_TRIES, atoi (val));
      else if (!strcasecmp (name, "pinentry-timeout"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_TIMEOUT, atoi (val));
      else if (!strcasecmp (name, "no-pinentry"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_NO_PINENTRY, !!(atoi (val)));
      else if (!strcasecmp (name, "local-pinentry"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_LOCAL_PINENTRY, !!(atoi (val)));
      else if (!strcasecmp (name, "pinentry-display"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_DISPLAY, val);
      else if (!strcasecmp (name, "pinentry-ttyname"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_TTY, val);
      else if (!strcasecmp (name, "pinentry-ttytype"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_TERM, val);
      else if (!strcasecmp (name, "pinentry-lc-messages"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_LC_MESSAGES, val);
      else if (!strcasecmp (name, "pinentry-lc-ctype"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_PINENTRY_LC_CTYPE, val);
      else if (!strcasecmp (name, "no-ssh-agent"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_SSH_AGENT, !(atoi (val)));
      else if (!strcasecmp (name, "no-tls-verify"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_TLS_VERIFY, !(atoi (val)));
      else if (!strcasecmp (name, "tls-priority"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_TLS_PRIORITY, val);
      else if (!strcasecmp (name, "socket-timeout"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_SOCKET_TIMEOUT, atoi (val));
      else if (!strcasecmp (name, "lock-timeout"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_LOCK_TIMEOUT, strtol (val, NULL, 10));
      else if (!strcasecmp (name, "no-lock"))
        rc = pwmd_setopt (pwm, PWMD_OPTION_LOCK_ON_OPEN, !(atoi (val)));
      else if (!strcasecmp (name, "include"))
        rc = set_rcdefaults (pwm, val);
      else
        fprintf(stderr, N_("%s(%u): invalid option '%s', ignored.\n"), f,
                line_n, name);

      if (rc)
        break;
    }

  fclose (fp);
  pwmd_free (line);
  pwmd_free (f);
  return rc;
}

gpg_error_t
pwmd_new (const char *name, pwm_t ** pwm)
{
  pwm_t *h = pwmd_calloc (1, sizeof (pwm_t));
  gpg_error_t rc;

  if (!h)
    return FINISH (GPG_ERR_ENOMEM);

  if (name)
    {
      h->name = pwmd_strdup (name);
      if (!h->name)
	{
	  pwmd_free (h);
	  return FINISH (GPG_ERR_ENOMEM);
	}
    }

  reset_handle (h);
  h->use_agent = -1;
  h->pinentry_timeout = -1;
  h->current_pinentry_timeout = -1;
  h->pinentry_tries = 3;
#if defined(WITH_SSH) || defined(WITH_GNUTLS)
  h->prot = PWMD_IP_ANY;
  h->socket_timeout = 120;
  h->tls_verify = 1;
#ifdef __MINGW32__
  WSADATA wsdata;

  WSAStartup (0x202, &wsdata);
#endif

#endif
  h->opts |= OPT_LOCK_ON_OPEN;

  rc = set_rcdefaults (h, NULL);
  if (rc)
    goto fail;

  *pwm = h;
  return 0;

fail:
  pwmd_close (h);
  return FINISH (rc);
}

void
pwmd_free (void *ptr)
{
  _xfree (ptr);
}

void *
pwmd_malloc (size_t size)
{
  return _xmalloc (size);
}

void *
pwmd_calloc (size_t nmemb, size_t size)
{
  return _xcalloc (nmemb, size);
}

void *
pwmd_realloc (void *ptr, size_t size)
{
  return _xrealloc (ptr, size);
}

char *
pwmd_strdup (const char *str)
{
  char *t;
  size_t len;
  register size_t c;

  len = strlen (str);
  t = _xmalloc ((len + 1) * sizeof (char));
  if (!t)
    return NULL;

  for (c = 0; c < len; c++)
    t[c] = str[c];

  t[c] = 0;
  return t;
}

char *
pwmd_strdup_printf (const char *fmt, ...)
{
  va_list ap, ap2;
  int len;
  char *buf;

  if (!fmt)
    return NULL;

  va_start (ap, fmt);
  va_copy (ap2, ap);
  len = vsnprintf (NULL, 0, fmt, ap);
  va_end (ap);
  buf = pwmd_malloc (++len);
  if (buf)
    vsnprintf (buf, len, fmt, ap2);

  va_end (ap2);
  return buf;
}

gpg_error_t
pwmd_getpin (pwm_t * pwm, const char *filename, char **result,
	     size_t * len, pwmd_pinentry_t which)
{
#ifndef WITH_PINENTRY
  return FINISH (GPG_ERR_NOT_IMPLEMENTED);
#else
  gpg_error_t rc;

  command_start (pwm);
  if (which == PWMD_PINENTRY_CONFIRM && pwm->disable_pinentry)
    {
      rc = get_password (pwm, NULL, NULL, which, 1);
      return FINISH (rc);
    }

  rc = _pwmd_getpin (pwm, filename, result, len, which);
  return FINISH (rc);
#endif
}

const char *
pwmd_version ()
{
  return LIBPWMD_VERSION_STR LIBPWMD_GIT_HASH;
}

unsigned int
pwmd_features ()
{
  unsigned int n = 0;

#ifdef WITH_PINENTRY
  n |= PWMD_FEATURE_PINENTRY;
#endif
#ifdef WITH_SSH
  n |= PWMD_FEATURE_SSH;
#endif
#ifdef WITH_QUALITY
  n |= PWMD_FEATURE_QUALITY;
#endif
#ifdef WITH_GNUTLS
  n |= PWMD_FEATURE_GNUTLS;
#endif
  return n;
}

gpg_error_t
pwmd_fd (pwm_t * pwm, int *fd)
{
  if (!pwm || !fd)
    return FINISH (GPG_ERR_INV_ARG);

  if (pwm->fd == ASSUAN_INVALID_FD)
    return FINISH (GPG_ERR_INV_STATE);

  *fd = HANDLE2SOCKET (pwm->fd);
  return 0;
}

void
pwmd_set_pointer (pwm_t *pwm, void *data)
{
  pwm->user_data = data;
}

void *
pwmd_get_pointer (pwm_t *pwm)
{
  return pwm->user_data;
}

int
pwmd_gnutls_error (pwm_t *pwm, const char **str)
{
#ifndef WITH_GNUTLS
  (void)pwm;
  (void)str;
  return 0;
#else
  if (str && pwm && pwm->tls_error)
    *str = gnutls_strerror (pwm->tls_error);

  return pwm ? pwm->tls_error : 0;
#endif
}

gpg_error_t
pwmd_cancel (pwm_t *pwm)
{
  if (!pwm)
    return FINISH (GPG_ERR_INV_ARG);

#if defined(WITH_SSH) || defined(WITH_GNUTLS)
  if (pwm->fd == ASSUAN_INVALID_FD && !pwm->tcp)
#else
  if (pwm->fd == ASSUAN_INVALID_FD)
#endif
    return FINISH (GPG_ERR_INV_STATE);

  /* Can only cancel the connection for the time being. */
  if (pwm->connected)
    return FINISH (GPG_ERR_INV_STATE);

  pwm->cancel = 1;
  return 0;
}

gpg_error_t
pwmd_test_quality (const char *str, double *result)
{
#ifndef WITH_QUALITY
  (void)str;
  (void)result;
  return GPG_ERR_NOT_IMPLEMENTED;
#else
  if (!result)
    return GPG_ERR_INV_ARG;

  *result = ZxcvbnMatch (str, NULL, NULL);
  return 0;
#endif
}

static gpg_error_t
do_pwmd_bulk_append (char **str, gpg_error_t *cmdrc, int with_rc,
                     const char *id, size_t idsize, const char *cmd,
                     const char *args, size_t argsize, size_t *offset)
{
  char buf[32];
  size_t len = 0, n = 0;
  char *s, *p;
  int sub = 0;
  char *rcbuf = NULL;

  if (!str || !id || !*id || (id && !idsize) || (idsize && !id)
      || !cmd || !*cmd || (args && *args && !argsize)
      || (argsize && (!args || !*args)))
    return FINISH (GPG_ERR_INV_ARG);

  if (*str && (strlen (*str) < 4 || memcmp (*str, "2:id", 4)))
    return FINISH (GPG_ERR_BAD_DATA);

  n = 4;
  snprintf (buf, sizeof (buf), "%zu:", idsize);
  n += strlen (buf) + idsize + 4;
  snprintf (buf, sizeof (buf), " %zu:", strlen (cmd));
  n += strlen (buf) + strlen (cmd);
  snprintf (buf, sizeof (buf), "%zu:", argsize);
  n += strlen (buf) + argsize;

  if (with_rc)
    {
      int i;
      char *t;

      for (i = 0; cmdrc[i] != GPG_ERR_MISSING_ERRNO; i++)
        {
          t = pwmd_strdup_printf ("%u|%s", cmdrc[i], rcbuf ? rcbuf : "");
          if (!t)
            {
              pwmd_free (rcbuf);
              return FINISH (GPG_ERR_ENOMEM);
            }

          pwmd_free (rcbuf);
          rcbuf = t;
        }

      if (rcbuf)
        {
          rcbuf[strlen (rcbuf)-1] = 0;
          t = pwmd_strdup_printf ("2:rc%u:%s", strlen (rcbuf), rcbuf);
          pwmd_free (rcbuf);
          if (!t)
            return FINISH (GPG_ERR_ENOMEM);

          rcbuf = t;
          n += strlen (rcbuf);
          sub = 1;
        }
    }

  if (*str)
    len = strlen (*str);

  if (offset && *offset < len)
    sub = 1;

  if (sub)
    n += 2; // Parentheses for this sub-command

  s = pwmd_malloc ((n + 1) * sizeof (char));
  if (!s)
    {
      pwmd_free (rcbuf);
      return FINISH (GPG_ERR_ENOMEM);
    }

  len = 0;
  if (with_rc && rcbuf)
    {
      memcpy (&s[len], rcbuf, strlen (rcbuf));
      len += strlen (rcbuf);
      pwmd_free (rcbuf);
    }

  if (sub)
    memcpy (&s[len++], "(", 1);

  snprintf (buf, sizeof (buf), "2:id%zu:", idsize);
  memcpy (&s[len], buf, strlen (buf));
  len += strlen (buf);
  memcpy (&s[len], id, idsize);
  len += idsize;

  snprintf (buf, sizeof (buf), "%zu:", strlen (cmd));
  memcpy (&s[len], buf, strlen (buf));
  len += strlen (buf);
  memcpy (&s[len], cmd, strlen (cmd));
  len += strlen (cmd);

  snprintf (buf, sizeof (buf), "%zu:", argsize);
  memcpy (&s[len], buf, strlen (buf));
  len += strlen (buf);
  if (args)
    memcpy (&s[len], args, argsize);
  len += argsize;

  if (sub)
    memcpy (&s[len++], ")", 1);

  s[len] = 0;

  n = *str ? strlen (*str) : 0;
  p = pwmd_realloc (*str, (n+len+1) * sizeof (char));
  if (!p)
    {
      pwmd_free (s);
      return FINISH (GPG_ERR_ENOMEM);
    }

  if (offset)
    {
      memmove (&p[*offset+len], &p[*offset], n - *offset);
      memcpy (&p[*offset], s, len);
      *offset += len;
    }
  else
    memcpy (&p[n], s, len);

  p[n+len] = 0;
  *str = p;

  pwmd_free (s);
  return FINISH (0);
}

gpg_error_t
pwmd_bulk_append (char **str, const char *id, size_t idsize, const char *cmd,
                  const char *args, size_t argsize, size_t *offset)
{
  return do_pwmd_bulk_append (str, 0, 0, id, idsize, cmd, args, argsize,
                              offset);
}

/* Consider the return code of the previous command and run the the specified
 * command when it matches. Creates the following syntax which trails the
 * previous commands syntax:
 *
 *    2:rc<R>:<rc>[|<rc>[...]](2:id...)
 *
 * To consider the return code for the previous command added with this
 * function, decrement offset by 1 to place the current command inside the
 * parentheses of the previous command.
 */
gpg_error_t
pwmd_bulk_append_rc (char **str, gpg_error_t *rc, const char *id,
                     size_t idsize, const char *cmd, const char *args,
                     size_t argsize, size_t *offset)
{
  return do_pwmd_bulk_append (str, rc, 1, id, idsize, cmd, args, argsize,
                              offset);
}

gpg_error_t
pwmd_bulk_finalize (char **str)
{
  size_t slen = 0;
  char *p;

  if (!str)
    return FINISH (GPG_ERR_INV_ARG);

  if (!*str || strlen (*str) < 4 || memcmp (*str, "2:id", 4))
    return FINISH (GPG_ERR_BAD_DATA);

  slen = strlen (*str);
  p = pwmd_realloc (*str, (slen+2+1) * sizeof (char));
  if (!p)
    return FINISH (GPG_ERR_ENOMEM);

  memmove (&p[1], p, slen);
  p[0] = '(';
  p[++slen] = ')';
  p[++slen] = 0;
  *str = p;
  return FINISH (0);
}

gpg_error_t
pwmd_bulk_result (const char *str, size_t size, const char *id, size_t id_size,
                  size_t *offset, const char **result, size_t *result_len,
                  gpg_error_t *cmd_rc)
{
  const char *s;
  size_t o;
  gpg_error_t rrc = 0;
  int match = 0;

  if (!str || !size || !id || !id_size || !offset)
    return FINISH (GPG_ERR_INV_ARG);

  s = str + *offset;
  o = *offset;

  if (result)
    *result = NULL;

  if (result_len)
    *result_len = 0;

  if (cmd_rc)
    *cmd_rc = 0;

  if (strlen (str) < 15 || memcmp (str, "(11:bulk-result", 15))
    return FINISH (GPG_ERR_BAD_DATA);
  else if (!o)
    {
      s += 15;
      o += 15;
    }

  for (;;)
    {
      size_t len;
      char *e = NULL;
      char buf[32];

      if (size - o < 4 || memcmp (s, "2:id", 4))
        return FINISH (GPG_ERR_NOT_FOUND);

      s += 4;
      o += 4;

      if (!isdigit (*s))
        return FINISH (GPG_ERR_SEXP_INV_LEN_SPEC);

      len = atoi (s);
      while (isdigit (*s))
        s++, o++;

      if (*s != ':')
        return FINISH (GPG_ERR_INV_SEXP);

      s++, o++;

      if (size - o < len)
        return FINISH (GPG_ERR_NOT_FOUND);

      /* Continue processing even though this command doesn't match. Commands
       * should be processed in the order the were added so this is safe to do.
       * If the command is not found then that means a previous command failed.
       * We can't do a strstr() or memmem() for the next "2:id" because command
       * result data may contain those bytes.
       */
      if (id_size == len && !memcmp (s, id, len))
        match = 1;

      s += len, o += len;

      if (!isdigit (*s))
        return FINISH (GPG_ERR_SEXP_STRING_TOO_LONG);

      if (size - o < 4 || memcmp (s, "2:rc", 4))
        return FINISH (GPG_ERR_INV_SEXP);

      s += 4, o += 4;
      if (!isdigit (*s))
        return FINISH (GPG_ERR_SEXP_INV_LEN_SPEC);

      e = NULL;
      len = strtoul (s, &e, 10);
      if (e && *e != ':')
        return FINISH (GPG_ERR_SEXP_INV_LEN_SPEC);

      if (len >= sizeof (buf))
        return FINISH (GPG_ERR_SEXP_STRING_TOO_LONG);

      while (isdigit (*s))
        s++, o++;

      if (*s != ':')
        return FINISH (GPG_ERR_INV_SEXP);

      s++, o++;
      strncpy (buf, s, len);
      buf[len] = 0;
      if (len > strlen (buf))
        return FINISH (GPG_ERR_SEXP_STRING_TOO_LONG);

      e = NULL;
      rrc = strtoul (buf, &e, 10);
      if (e && *e)
        return FINISH (GPG_ERR_SEXP_INV_LEN_SPEC);

      s += len;
      o += len;

      // Data length
      e = NULL;
      len = strtoul (s, &e, 10);
      if (e && *e != ':')
        return FINISH (GPG_ERR_SEXP_INV_LEN_SPEC);

      while (isdigit (*s))
        s++, o++;

      if (*s != ':')
        return FINISH (GPG_ERR_INV_SEXP);

      s++, o++;
      o += len;

      // If no match, continue searching for the command id.
      if (match)
        {
          if (result)
            *result = s;

          if (result_len)
            *result_len = len;

          if (cmd_rc)
            *cmd_rc = rrc;

          *offset = o;
          return FINISH (0);
        }
    }

  return FINISH (GPG_ERR_NOT_FOUND);
}
