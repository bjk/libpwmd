/*
    Copyright (C) 2018-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License version 2.1 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <stdio.h>

char *strsep (char **src, const char *sep)
{
  char *p;
  char *ret = *src;

  if (!ret)
    return NULL;

  for (p = *src; p && *p; p++)
  {
    const char *s;

    for (s = sep; s && *s; s++)
    {
      if (*s == *p)
      {
	*src = p+1;
	*p = 0;
	return ret;
      }
    }
  }

  p = *src;
  *src = NULL;
  return p;
}
