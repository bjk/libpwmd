libpwmd supports remote access to a pwmd server via TLS and SSH by using the
libssh2 library. Libssh2 supports being built with various crypto engines and
libpwmd has been tested and works well when libssh2 is built with the OpenSSL
crypto engine. Other engines may or may not work.

The pwmd_connect() function is used to connect to the remote pwmd host by
prefixing the socket with ssh:// followed by an optional username then the
hostname.  After resolving the hostname and connecting, an ssh session is
created and the hostkey is verified against a file containing known hosts.

After verification succeeds, a channel is opened which spawns a shell. This
shell should execute a proxy to the pwmd server by connecting to the local
socket. I use 'socat', it's really handy.

In order to get this to work you need an SSH server that supports this
feature. OpenSSH works well. Then put the following in your
~/.ssh/authorized_keys file on the machine running the ssh and pwmd server. It
should be prepended to the public key portion of the private key that your
libpwmd client will use:

    command="socat UNIX-CONNECT:$HOME/.pwmd/socket -" ... identity.pub ...

Now when libpwmd connects, the SSH server spawns the shell and 'socat' will
send the SSH connection IO to the pwmd socket, and vice-versa.

You can use pwmc to try it out:
    # Generate an SSH key that the client will use. Then copy the contents of
    # the generated public key to the SSH servers authorized_keys file and
    # prepend the above mentioned line to it.
    ssh-keygen -f keyfile

    # If you have an ssh agent running, you can also do the following and omit
    # the --identity command line option below:
    ssh-add keyfile

    # List the contents of the pwmd 'datafile' on the remote SSH server by
    # connecting as the specified 'user'. If you have an ssh agent running and
    # did the above command, you can omit the --identity option.
    echo list | pwmc --socket ssh://user@hostname --identity keyfile datafile


Ben Kibbey <bjk@luxsci.net>
https://gitlab.com/bjk/libpwmd
